package com.utils {
	import org.osmf.net.StreamingURLResource;

	public class DataUtils {

		public static function isValidEmail($mail:String):Boolean {
			var emailExpression:RegExp = /^[a-zA-Z0-9_.-]{2,}@[a-zA-Z0-9_-]{2,}\.[a-zA-Z]{2,4}(\.[a-zA-Z]{2,4})?$/;
			return emailExpression.test($mail);
		}

		public static function isValidDateAAAA_MM_DD($date:String, $separator:String = "-", $minYear:uint = 1, $maxYear:uint = 2014):Boolean {
			var dateExpression:RegExp = /^[0-9]{4}[-\.\/]{1}[0-9]{2}[-\.\/]{1}[0-9]{2}?$/;
			if(!dateExpression.test($date)) {
				return false;
			}
			var data:Array = $date.split($separator);
			if(!MathUtils.isValueBetween(uint(data[0]), $minYear - 1, $maxYear + 1) || !MathUtils.isValueBetween(uint(data[1]), 0, 13) || !MathUtils.isValueBetween(uint(data[2]), 0, 32)) {
				return false;
			}
			return true;
		}

	}
}