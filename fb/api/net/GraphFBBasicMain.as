package net {

	import com.core.BasicMain;
	import com.core.system.System;
	import com.facebook.graph.Facebook;
	import com.interfaces.IClient;
	import com.ui.AlertEvent;
	import com.ui.controllers.SiteAlertManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.net.LocalConnection;
	import flash.utils.Timer;
	
	import net.core.FacebookGraph;
	import net.events.FacebookConnectLoginEvent;
	import net.structures.DetailFBUserData;

	public class GraphFBBasicMain extends BasicMain implements IClient {

		protected var _facebook:FacebookConnect;
		protected var _timeout:Timer;
		protected var _permissions:Array = [FacebookConnect.USER_PHOTOS, FacebookConnect.EMAIL];

		public function GraphFBBasicMain($url:String="./swf/v1_0/config/localURLSData.xml") {
			super($url);
		}

		override protected function onGraphicsLoaded($event:Event):void {
			super.onGraphicsLoaded($event);
			initFBConnection();
		}

		protected function initFBConnection():void {
			if((new LocalConnection()).domain == "localhost") {
				FacebookGraph.getInstance().appID = "263107520380920";
				_facebook = new FacebookConnect(FacebookGraph.getInstance().appID);
				if(_serviceTestMode) {
					FacebookGraph.getInstance().userData = new DetailFBUserData({first_name:"Simkin", gender:"male", hometown:{}, id:"100000293224144",
																last_name:"Jerkin", link:"http://www.facebook.com/profile.php?id=100000293224144",
																locale:"es_LA", name:"Simkin Jerkin", timezone:-6, updated_time:"2011-10-24T22:19:14+0000",
																verified:true, work:[]});
					FacebookGraph.getInstance().accessToken = "AAAGfkBzRsrwBAPJdLlVaZCymtF6hMgEGxvoJIzp6AwGxoZChmExgo1cfDny6UXnxWhb2FRLDTgnCBZBYOZCYmTbpMVVACX2OUXAmvrPJiFxWrnChkl7p";					_facebook.fbUserData = FacebookGraph.getInstance().userData;
					onLoginSuccess(null);
					return;
				}
				sendLogin();
				//createTimer();
			} else {
				System.addDebugMessage("APP_ID : " + getAppID());
				if(getAppID() != "") {
					FacebookGraph.getInstance().appID = getAppID();
					_facebook = new FacebookConnect(FacebookGraph.getInstance().appID, System.loaderInfoRoot.parameters.access_token);
					sendLogin();
					//createTimer();
				}
			}
		}

		protected function getAppID():String {
			if(System.loaderInfoRoot.parameters.app_id) {
				return System.loaderInfoRoot.parameters.app_id;
			}
			return "";
		}

		protected function loginListeners($add:Boolean = true):void{
			addListener(_facebook, FacebookConnectLoginEvent.USER_LOGIN_SUCCES, onLoginSuccess, $add);
			addListener(_facebook, FacebookConnectLoginEvent.USER_LOGIN_FAIL, onLoginFail, $add);
		}

		protected function createTimer():void {
			/*
			_timeout = new Timer(10000, 1);
			addListener(_timeout, TimerEvent.TIMER, onLoginTimerComplete);
			_timeout.start();
			*/
		}

		protected function destructTimer():void {
			if(_timeout) {
				_timeout.stop();
				_timeout = null;
			}
		}

		protected function onLoginTimerComplete($event:TimerEvent):void {
			SiteAlertManager.getInstance().showAlert("Facebook timer out", ["OK"], onLoginAlertClose);
		}

		protected function onLoginAlertClose($event:AlertEvent):void {
			sendLogin();
			createTimer();
		}

		protected function sendLogin():void {
			loginListeners();
			_facebook.facebookLogin(_permissions);
		}

		protected function onLoginSuccess($event:FacebookConnectLoginEvent):void {
			loginListeners(false);
			destructTimer();
			SiteAlertManager.getInstance().closeAlert();
			if(!FacebookGraph.getInstance().userData) {
				var data:Object = _facebook.dataObject;
				FacebookGraph.getInstance().userData = new DetailFBUserData(_facebook.dataObject as Object);
			}
			transactionComplete();
		}

		protected function onLoginFail($event:FacebookConnectLoginEvent):void{
			loginListeners(false);
			SiteAlertManager.getInstance().showAlert("No se ha obtenido sesión de Facebook. Presiona OK para intentar otra vez.", ["OK"], onLoginAlertClose);
		}
		
		protected function transactionComplete():void{
			trace("Login de Facebook exitoso");
		}
	}
}