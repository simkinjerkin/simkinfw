package net.graphics {

	import com.graphics.gallery.IThumbnail;
	import com.graphics.gallery.Thumbnail;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.system.ImageDecodingPolicy;
	import flash.text.TextField;

	public class PhotoThumbnail extends Thumbnail implements IThumbnail {

		public function PhotoThumbnail($ID:String, $background:DisplayObject, $imageUrl:String="", $title:TextField=null) {
			super($ID, $background, $imageUrl, $title);
		}

		override protected function setImage($image:DisplayObject):void {
			super.setImage($image);
			_image.mask = (_background as MovieClip).thumbMask;
		}

		override protected function setSizeImage():void {
			if(_adjustImage) {
				var scale:Number = Math.max(_background.width / _image.width, _background.height / _image.height);
				_image.scaleX = _image.scaleY = scale;
				_image.x = (_background.width - _image.width) / 2;
				_image.y = (_background.height - _image.height) / 2;
			}
		}

	}

}