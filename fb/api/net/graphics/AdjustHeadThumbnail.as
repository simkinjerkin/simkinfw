package net.graphics {

	import com.geom.ComplexPoint;
	import com.graphic.RotatableDynamicSprite;
	import com.graphics.gallery.IThumbnail;
	import com.graphics.gallery.IThumbnailData;
	import com.graphics.gallery.Thumbnail;
	import com.utils.GraphicUtils;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	public class AdjustHeadThumbnail extends Thumbnail implements IThumbnail {

		protected var _ds:RotatableDynamicSprite;
		protected var _mask:DisplayObject;
		protected var _originDO:DisplayObject;
		protected var _headBounds:Rectangle;
		protected var _maskBounds:Rectangle;
		protected var _loader:MovieClip;
		protected var _flipPhoto:Boolean = false;
		protected var _lastRotation:Number = 0;
		protected var _dynamicSpriteClass:Class = RotatableDynamicSprite;
		
		public function set dynamicSpriteClass($value:Class):void {
			_dynamicSpriteClass = $value;
		}

		public function get dynamicImage():RotatableDynamicSprite {
			return _ds;
		}

		protected function get graphicMovie():MovieClip {
			return image.graphic;
		}

		override public function set data($value:IThumbnailData):void {
			destructCanvas();
			super.data = $value;
		}

		public function get image():MovieClip {
			return (_image as MovieClip);
		}

		public function get headBounds():Rectangle {
			return _headBounds;
		}

		protected function get head():MovieClip {
			return image.head;
		}

		public function AdjustHeadThumbnail($ID:String, $background:DisplayObject, $imageUrl:String="", $title:TextField=null) {
			super($ID, $background, $imageUrl, $title);
		}

		public function getImage():DisplayObject {
			if(_headBounds){
				return GraphicUtils.getCloneRectAt(this, _headBounds.x, _headBounds.y, _headBounds.width, _headBounds.height);
			}else{
				return new Sprite();
			}
		}

		public function setUserImage($do:DisplayObject, $headLimits:Rectangle, $webcam:Boolean = false, $x:Number = 0, $y:Number = 0, $maskBounds:Rectangle = null):void {
			destructCanvas();
			_ds = new _dynamicSpriteClass();
			_maskBounds = $maskBounds;
			initMask($headLimits);
			_ds.borderEnabled = false;
			_ds.rotateRegister = new ComplexPoint((_headBounds.x + (_headBounds.width / 2)), _headBounds.y + (_headBounds.height / 2));
			_ds.resizeRegister = new ComplexPoint((_headBounds.x + (_headBounds.width / 2)), _headBounds.y + (_headBounds.height / 2));
			if(!$webcam) {
//				_ds.limits = new Rectangle(_headBounds.right - $do.width, _headBounds.bottom - $do.height, -(_headBounds.right - $do.width) + $do.width + _headBounds.x, -(_headBounds.bottom - $do.height) + $do.height + _headBounds.y);
			} else {
//				_ds.limits = new Rectangle(20, $y, $do.width, $do.height);
			}
			_originDO = $do;
			addDo();
			addElement(_ds);
			addElement(_mask);
			_ds.mask = _mask;
		}

		protected function addDo():void {
			if(_ds) {
				if(_flipPhoto) {
					_ds.addChild(GraphicUtils.getInverseClone(_originDO));
					if(_ds.numChildren > 1) {
						_ds.removeChild(_ds.getChildAt(0));
					}
				} else {
					_ds.addChild(_originDO);
				}
			}
		}

		public function flipPhoto():void {
			if(_ds) {
				_flipPhoto = !_flipPhoto;
				addDo();
			}
		}

		public function resizeUserImage($value:Number):void {
			if(_ds) {
				_ds.scaleX = _ds.scaleY = $value;
				updateLimits();
			}
		}

		public function updateLimits():void {
			if(_ds) {
				var delta:Number = _lastRotation != 0 ? (100 * _ds.scaleX) : 0;
				var currentW:Number = (_ds.origin.width * _ds.scaleX) + delta;
				var currentH:Number = (_ds.origin.height * _ds.scaleY) + delta;
//				_ds.limits = new Rectangle(_headBounds.right - currentW, _headBounds.bottom - currentH,
//											-(_headBounds.right - currentW) + currentW + _headBounds.x,
//											-(_headBounds.bottom - currentH) + currentH + _headBounds.y);
				return;
				var absRot:Number = _lastRotation % 90;
				var percent:Number = absRot / 90;
				var alteredW:Number = currentW + ((currentH - currentW) * percent);
				var alteredH:Number = currentH + ((currentW - currentH) * percent);
				trace(_ds.rotation);
				trace(currentW + " ... " + currentH);
				trace(alteredW + " ::: " + alteredH);
				var updateX:Number = _headBounds.right - alteredW;
				var updateY:Number = _headBounds.bottom - alteredH;
				var updateW:Number = -(_headBounds.right - alteredW) + alteredW + _headBounds.x;
				var updateH:Number = -(_headBounds.bottom - alteredH) + alteredH + _headBounds.y;
				trace(_ds.limits);
//				return;
				_ds.limits = new Rectangle(updateX, updateY, updateW, updateH);
			}
		}

		public function rotateUserImage($value:Number):void {
			if(_ds) {
				trace("rotateUserImage ::: " + $value);
				_ds.rotation = $value;
				_lastRotation = $value;
				updateLimits();
			}
		}

		public function destruct():void {
			destructCanvas();
		}

		override protected function getFaceButton():DisplayObject {
			return createFaceButton(1, 1, 0x000000, 0);
		}

		protected function destructCanvas():void {
			if(_ds) {
				addElement(_mask, false);
				_mask = null;
				_ds.destruct();
				addElement(_ds, false);
				_ds = null;
			}
		}

		protected function initMask($headLimits:Rectangle):void {
			if(!_mask) {
				_headBounds = $headLimits;
				_mask = getImageMask();
			}
		}

		override protected function loadImage($imageURL:String):void {
			showLoader();
			super.loadImage($imageURL);
		}

		override protected function onLoadImage($event:Event):void {
			showLoader(false);
			super.onLoadImage($event);
		}

		protected function showLoader($show:Boolean = true):void {
			if(!_loader){
				_loader = new GraphicInfinitLoader();
				_loader.x = -220;
				_loader.y = 200;
			}
			addElement(_loader, $show);		
		}

		protected function getImageMask():DisplayObject {
			var mask:Sprite = new Sprite();
			mask.graphics.beginFill(0, 0);
			var bounds:Rectangle = _maskBounds ? _maskBounds : _headBounds;
			mask.graphics.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
			mask.graphics.endFill();
			return mask;
		}

		protected function get alertsContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("alertsContainer");
		}

	}
}