package net.core {

	import net.structures.DetailFBUserData;

	public class FacebookGraph {

		protected var _userData:DetailFBUserData;
		protected var _appID:String = "";
		protected var _accessToken:String = "";

		private static var _instance:FacebookGraph;

		public function get accessToken():String {
			return _accessToken;
		}
		
		public function set accessToken($value:String):void {
			_accessToken = $value;
		}

		public function get appID():String {
			return _appID;
		}
		
		public function set appID($value:String):void {
			_appID = $value;
		}

		public function get userData():DetailFBUserData {
			return _userData;
		}

		public function set userData($value:DetailFBUserData):void {
			_userData = $value;
		}

		public function FacebookGraph($newCall:Function = null) {
			if ($newCall != FacebookGraph.getInstance) {
				throw new Error("FacebookGraph");
			}
			if (_instance != null)			{
				throw new Error("FacebookGraph");
			}
		}

		public static function getInstance():FacebookGraph {
			if (_instance == null )	{
				_instance = new FacebookGraph (arguments.callee);
			}
			return _instance;
		}

	}
}