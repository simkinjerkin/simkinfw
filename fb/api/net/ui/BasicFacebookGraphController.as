package net.ui {

	import com.core.system.System;
	import com.facebook.graph.Facebook;
	import com.net.BasicLoaderEvent;
	import com.ui.controllers.BasicTransactionController;
	
	import flash.display.Sprite;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import net.core.FacebookGraph;
	
	public class BasicFacebookGraphController extends BasicTransactionController {

		public function BasicFacebookGraphController($container:Sprite) {
			super($container);
		}

		protected function streamPublish($message:String = "", $picture:String = "", $link:String = "", $name:String = "", $caption:String = "", $desc:String = "", $source:String = "", $profileID:String = ""):void {
			$profileID = $profileID == "" ? FacebookGraph.getInstance().userData.ID : $profileID;
			var args:Array = arguments;
			System.addDebugMessage(args.toString());
			Facebook.ui("stream.publish", {name:$name, link:$link, picture:$picture, caption:$caption, description:$desc}, onPostResponse, "popup");
		}

		protected function onPostResponse($result:Object):void {
			if($result) {
				trace("Result");
			} else {
				trace("Fail");
			}
		}

	}
}