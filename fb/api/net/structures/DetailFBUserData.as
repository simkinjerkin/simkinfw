package net.structures {

	import com.graphics.gallery.IThumbnailData;

	public class DetailFBUserData extends FBUserData implements IThumbnailData {

		protected var _education:Array;
		protected var _firstName:String;
		protected var _gender:String;
		protected var _hometown:Object;
		protected var _lastName:String;
		protected var _locale:String;
		protected var _location:Object;
		protected var _mail:String = "";
		protected var _timezone:int;
		protected var _updated:String;
		protected var _username:String;
		protected var _verified:Boolean;
		protected var _work:Array;

		public function get firstName():String {
			return _firstName;
		}
		
		public function get lastName():String {
			return _lastName;
		}
		
		public function get gender():String {
			return _gender;
		}

		public function get name():String {
			return imageName;
		}

		public function get detail():Boolean {
			return true;
		}

		public function get education():Array {
			return _education;
		}

		public function get hometown():Object {
			return _hometown;
		}

		public function get location():Object {
			return _location;
		}

		public function get mail():String {
			return _mail;
		}

		public function get work():Array {
			return _work;
		}
		
		public function get userName():String {
			return _username;
		}

		public function DetailFBUserData($data:Object, $index:uint = 0) {
			super($data, $index);
			_education = $data.education;
			if($data.education) {
				trace("Education ::: " + _name);
			}
			_firstName = $data.first_name;
			if($data.gender) {
				_gender = $data.gender == "male" ? "H" : "F";
			}
			_hometown = $data.hometown;
			_lastName = $data.last_name;
			if($data.email) {
				_mail = $data.email;
			}
			thumbnailURL = $data.link;
			_locale = $data.locale;
			_location = $data.location;
			_timezone = $data.timezone;
			_updated = $data.updated_time;
			_username = $data.username;
			_verified = $data.verified;
			_work = $data.work;
		}
	}
}