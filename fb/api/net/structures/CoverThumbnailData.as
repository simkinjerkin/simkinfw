package net.structures {

	import com.graphics.gallery.IThumbnailData;
	import com.graphics.gallery.ThumbnailData;

	public class CoverThumbnailData extends ThumbnailData implements IThumbnailData {

		protected var _offsetY:Number = 0;

		public function get offsetY():Number {
			return _offsetY;
		}

		public function CoverThumbnailData($data:Object) {
			super($data.id, $data.name);
			_offsetY = $data.offset_y;
			thumbnailURL = $data.source;
		}

	}
}