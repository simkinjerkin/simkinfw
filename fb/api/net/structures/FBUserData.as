package net.structures {

	import com.graphics.gallery.IThumbnailData;

	import flash.display.DisplayObject;

	public class FBUserData extends IDData implements IThumbnailData {

		protected var _index:uint;
		protected var _type:String = "";
		protected var _imageWidth:Number = 0;
		protected var _imageHeight:Number = 0;
		protected var _cover:CoverThumbnailData;
		protected var _friends:Array;
		protected var _photos:Array;
		protected var _feed:Array;

		public function get feed():Array {
			return _feed;
		}

		public function set feed($value:Array):void {
			_feed = $value;
		}

		public function get photos():Array {
			return _photos;
		}

		public function set photos($value:Array):void {
			_photos = $value;
		}

		public function get friends():Array {
			return _friends;
		}

		public function set friends($value:Array):void {
			_friends = $value;
		}

		public function get imageWidth():Number {
			return _imageWidth;
		}

		public function get imageHeight():Number {
			return _imageHeight;
		}

		public function set imageWidth($value:Number):void {
			_imageWidth = $value;
		}

		public function set imageHeight($value:Number):void {
			_imageHeight = $value;
		}

		public function get cover():CoverThumbnailData {
			return _cover;
		}

		public function set cover($value:CoverThumbnailData):void {
			_cover = $value;
		}

		public function get type():String {
			return _type;
		}

		public function set type($value:String):void {
			_type = $value;
		}

		public function get index():uint {
			return _index;
		}

		public function set thumbnailURL($value:String):void {}
		public function set URL($value:String):void {}
		public function set data($data:Object):void {}
		public function set thumbnail($value:DisplayObject):void {}
		public function get thumbnail():DisplayObject {return null;}

		public function get thumbnailURL():String {
			return "https://graph.facebook.com/" + _ID + "/picture";
		}

		public function get URL():String {
			return "https://graph.facebook.com/" + _ID + "/picture?type=large";
		}

		public function FBUserData($data:Object, $index:uint = 0) {
			super($data);
			_index = $index;
			if($data.picture) {
				thumbnailURL = $data.picture;
			}
			if($data.cover) {
				_cover = new CoverThumbnailData($data.cover);
			}
		}

	}
}