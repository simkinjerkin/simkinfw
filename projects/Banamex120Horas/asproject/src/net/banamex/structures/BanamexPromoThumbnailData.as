package net.banamex.structures {
	import com.core.config.URLManager;
	import com.graphics.gallery.ThumbnailData;
	
	public class BanamexPromoThumbnailData extends ThumbnailData {
		
		protected var _providerID:String;
		protected var _promoText:String;
		protected var _promoURL:String;
		protected var _discount:String;
		protected var _highlighted:String;
		protected var _promoImage:String;
		protected var _slug:String;
		
		public function get providerID():String {
			return _providerID;
		}

		public function get promoText():String {
			return _promoText;
		}

		public function get promoURL():String {
			if(!_promoURL) {
				return URLManager.getInstance().getPath("serviceBase") + "#";
			}
			return _promoURL;
		}

		public function get discount():String {
			return _discount;
		}
		
		public function get highlighted():String {
			return _highlighted;
		}

		public function get promoImage():String {
			return _promoImage;
		}

		public function get slug():String {
			return _slug;
		}

		public function BanamexPromoThumbnailData($ID:String="", $imageName:String="", $data:Object = null) {
			super($ID, $imageName);
			if($data) {
				_providerID = $data.partner_id;
				_discount = $data.discount;
				_highlighted = $data.highlighted;
				_promoText = $data.description;
				_promoURL = $data.url;
				_thumbnailURL = URLManager.getInstance().getPath("mediaBase") + $data.partner_image;
				_promoImage = $data.image;
				_slug = $data.slug;
			}
		}
	}
}