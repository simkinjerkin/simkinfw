package net.banamex.structures {

	public class LoggedUserData {

		protected var _name:String;
		protected var _mail:String;
		protected var _gender:String;
		protected var _registered:Boolean;
		protected var _client:Boolean = false;

		public function get name():String {
			return _name;
		}

		public function get mail():String {
			return _mail;
		}

		public function get gender():String {
			return _gender;
		}

		public function get registered():Boolean {
			return _registered;
		}

		public function set client($value:Boolean):void {
			_client = $value;
		}

		public function get client():Boolean {
			return _client;
		}

		public function LoggedUserData($name:String, $mail:String, $gender:String, $registered:Boolean = false) {
			_name = $name;
			_mail = $mail;
			_gender = $gender;
			_registered = $registered
		}
	}
}