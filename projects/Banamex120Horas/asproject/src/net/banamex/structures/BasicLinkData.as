package net.banamex.structures {

	public class BasicLinkData extends BasicData {

		protected var _link:String;
		protected var _created:String;
		protected var _updated:String;

		public function get link():String {
			return _link;
		}

		public function BasicLinkData($data:Object) {
			super($data);
			if($data.link) {
				_link = $data.link;
			} else if($data.href) {
				_link = $data.href;
			}
			_created = $data.created_date;
			_updated = $data.updated_time;
		}

	}
}