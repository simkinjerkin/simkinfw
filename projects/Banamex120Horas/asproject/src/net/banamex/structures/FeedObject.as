package net.banamex.structures {

	import com.graphics.gallery.IThumbnailData;
	
	import flash.display.FrameLabel;
	
	import net.banamex.structures.BasicData;
	import net.banamex.structures.BasicFeedObject;
	import net.core.FacebookGraph;

	public class FeedObject extends BasicFeedObject implements IThumbnailData {

		public static const LIKE:String = "like";
		public static const LINK:String = "link";
		public static const PHOTO:String = "photo";
		public static const STATUS:String = "status";

		protected var _application:BasicData;
		protected var _caption:String;
		protected var _iconURL:String;
		protected var _message:String;
		protected var _messageTags:Object;
		protected var _objectID:String;
		protected var _privacy:Object;
		protected var _story:String;
		protected var _storyTags:Object;
		protected var _type:String;
		protected var _objectType:String;
		protected var _biographyObject:Boolean = false;

		override public function get thumbnailURL():String {
			return _pictureURL;
		}

		public function get objectType():String {
			return _objectType;
		}

		public function FeedObject($data:Object) {
			super($data);
			if($data.application) {
				_application = new BasicData($data.application);
			}
			_caption = $data.caption;
			_iconURL = $data.icon;
			_message = $data.message;
			_messageTags = $data.message_tags;
			_objectID = $data.object_id;
			_privacy = $data.privacy;
			_story = $data.story;
			_storyTags = $data.story_tags;
			_type = $data.type;
			setObjectType($data);
			trace(" T: " + _type + ", OType: " + _objectType + " ::: " + ($data.story ? _story : _message));
			var isStatusMessage:Boolean = _type == STATUS && _from.ID == FacebookGraph.getInstance().userData.ID && $data.message;
			var isStatusLink:Boolean = _type == LINK && $data.message;
			if(_type == PHOTO || isStatusMessage || isStatusLink) {
				_biographyObject = true;
				trace("Se publica en biografia");
			}
		}

		protected function setObjectType($data:Object):void {
			setStatusObjectType($data);
		}

		protected function setStatusObjectType($data:Object):void {
			if($data.story) {
				if(_story.indexOf("like") != -1) {
					_objectType = LIKE;
				}
			}
		}

	}
}