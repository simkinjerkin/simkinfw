package net.banamex.structures {

	import com.graphics.gallery.IThumbnailData;
	
	import flash.display.DisplayObject;

	public class BasicFeedObject extends BasicLinkData implements IThumbnailData {

		protected var _from:BasicData;
		protected var _pictureURL:String;

		public function get imageName():String {
			return _name;
		}
		
		public function set imageName($value:String):void {
			_name = $value;
		}
		
		public function get thumbnail():DisplayObject {
			return null;
		}
		
		public function set thumbnail($value:DisplayObject):void {}

		public function get thumbnailURL():String {
			return _link;
		}
		
		public function set thumbnailURL($value:String):void {}
		
		public function get URL():String {
			return _link;
		}
		
		public function set URL($value:String):void {
			_link = $value;
		}
		
		public function set data($value:Object):void {}

		public function BasicFeedObject($data:Object) {
			super($data);
			_from = new BasicData($data.from);
			_pictureURL = $data.picture;

		}

	}
}