package net.banamex.structures {

	public class BasicData {

		protected var _ID:String;
		protected var _name:String;
		protected var _desc:String;

		public function get ID():String {
			return _ID;
		}

		public function set ID($value:String):void {
			_ID = $value;
		}

		public function get name():String {
			return _name;
		}
		
		public function set name($value:String):void {
			_name = $value;
		}

		public function get desc():String {
			return _desc;
		}

		public function set desc($value:String):void {
			_desc = $value;
		}

		public function BasicData($data:Object) {
			_ID = $data.id;
			_name = $data.name;
			if($data.desc) {
				_desc = $data.desc;
			} else if($data.text) {
				_desc = $data.text;
			}
		}

	}
}