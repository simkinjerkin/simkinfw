package net.banamex.structures {

	import com.graphics.gallery.IThumbnailData;

	import flash.display.DisplayObject;

	public class PhotoObject extends BasicFeedObject implements IThumbnailData {

		protected var _height:Number;
		protected var _icon:String;
		protected var _images:Array;
		protected var _position:Number;
		protected var _source:String;
		protected var _tags:Object;
		protected var _width:Number;

		override public function get thumbnailURL():String {
			return _images[6].source;
		}

		override public function get URL():String {
			return _source;
		}

		override public function set URL($value:String):void {
			_source = $value;
		}

 		public function PhotoObject($data:Object) {
			super($data);
			_height = $data.height;
			_icon = $data.icon;
			_images = $data.images;
			_position = $data.position;
			_source = $data.source;
			_tags = $data.tags;
			_width = $data.width;
		}

	}
}