package net.banamex {

	import com.core.config.SystemConfigurationManager;
	import com.core.system.System;
	import com.facebook.graph.Facebook;
	import com.google.tracking.AnalyticManager;
	import com.ui.controllers.SiteAlertManager;
	
	import flash.events.Event;
	import flash.events.SyncEvent;
	import flash.net.LocalConnection;
	import flash.utils.Dictionary;
	
	import net.GraphFBBasicMain;
	import net.banamex.controllers.Banamex120HoursBackController;
	import net.banamex.controllers.Banamex120HoursHomeController;
	import net.banamex.controllers.Banamex120HoursShareController;
	import net.banamex.controllers.PicturePickerController;
	import net.events.FacebookConnectLoginEvent;
	
	public class Banamex120HoursMain extends GraphFBBasicMain {

		protected var _backgroundController:Banamex120HoursBackController;
		protected var _homeController:Banamex120HoursHomeController;
		protected var _picturePicker:PicturePickerController;
		protected var _shareController:Banamex120HoursShareController;

		public function Banamex120HoursMain($url:String="./swf/v1_0/config/localURLSData.xml") {
			super($url);
//			SoundMixer.soundTransform = new SoundTransform(.05);
//			_serviceTestMode = true;
		}

		override protected function setPreloadMovies():void {
			if(System.loaderInfoRoot.parameters.GAID) {
				AnalyticManager.getInstance().setTracking(System.stageRoot, System.loaderInfoRoot.parameters.GAID,
															"AS3", !SystemConfigurationManager.getInstance().configurationData.enabledTracking);
			}
//			System.addDebugMessage(System.loaderInfoRoot.parameters.videoID + " ::: " + System.loaderInfoRoot.parameters.bannerName);
//			SiteAlertManager.getInstance().showLoader(true, "Cargando gráfico", System.stageRoot.stageWidth / 2, System.stageRoot.stageHeight / 2);
//			_graphicsLoader.loadRequest("back", "Background.swf");
		}

		override protected function loadGraphics():void {
			_graphicsLoader.loadRequest("back", "Background.swf");
			_graphicsLoader.loadRequest("home", "banamexHome.swf");
			_graphicsLoader.loadRequest("picPicker", "PicturePicker.swf");
			_graphicsLoader.loadRequest("shareScreen", "shareScreen.swf");
			super.loadGraphics();
		}

		override protected function onGraphicsLoaded($event:Event):void {
			super.onGraphicsLoaded($event);
			showBackground();
		}

		override protected function onLoginSuccess($event:FacebookConnectLoginEvent):void {
			super.onLoginSuccess($event);
			showHome();
			AnalyticManager.getInstance().setTrackEvent('home', 'load', 'home');
		}

		protected function showBackground($show:Boolean = true):void {
			if(!_backgroundController){
				_backgroundController = new Banamex120HoursBackController(_graphicsLoader.library.back);
				_backgroundController.showScreen(false);
			}
			addElement(_backgroundController.container, $show, mainContainer);
		}

		protected function showHome($show:Boolean = true):void {
			if(!_homeController){
				_homeController = new Banamex120HoursHomeController(_graphicsLoader.library.home, _serviceTestMode);
			}
			addListener(_homeController, Banamex120HoursHomeController.ON_CONTINUE_CLICK, onContinueClick, $show);
			addElement(_homeController.container, $show, mainContainer);
		}
		
		protected function showPicturePicker($add:Boolean = true):void {
			showHome(false);
			_backgroundController.showScreen($add);
			if(!_picturePicker) {
				_picturePicker = new PicturePickerController(_graphicsLoader.library.picPicker, _graphicsLoader.library.back.screen.photoContainer);
			}
			if($add) {
				_picturePicker.reinit();
			}
			addListener(_picturePicker, PicturePickerController.ON_CELEBRATION_MADE, onContinueClick, $add);
			addListener(_picturePicker, PicturePickerController.ON_HEAD_ADJUST, onContinueClick, $add);
			addListener(_picturePicker, PicturePickerController.HIDE_SHARE, onContinueClick, $add);
			addListener(_picturePicker, PicturePickerController.ON_HOME_CLICK, onContinueClick, $add);
			addElement(_picturePicker.container, $add, mainContainer);
		}
		
		protected function onContinueClick($event:Event):void {
			switch($event.type){
				case PicturePickerController.ON_HEAD_ADJUST: 	showShare();		return;
				case PicturePickerController.HIDE_SHARE: 		showShare(false);	return;
			}
			switch($event.currentTarget){
				case _shareController:	showShare(false);
										showPicturePicker();
				case _homeController:	showPicturePicker();	break;
				case _picturePicker:	showPicturePicker(false);
										_homeController.addArrowButtons();
										_homeController.hideArrowsButton(false);
										showHome();				break;
			}
		}

		protected function onChangeImageButton($event:Event):void {
			_picturePicker.doBackButtonAction();
		}

		protected function showShare($show:Boolean = true):void {
			if(!_shareController){
				_shareController = new Banamex120HoursShareController(_graphicsLoader.library.shareScreen,  _graphicsLoader.library.back.screen);
			}
			if($show) {
				_shareController.promotion = _homeController.randomPromotion;
			}
			($show) ? _shareController.addImage(_picturePicker.userImage) : "";
			addListener(_shareController, Banamex120HoursShareController.ON_CHANGE_BUTTON, onChangeImageButton, $show);
			addElement(_shareController.container, $show, mainContainer);
		}

	}
}