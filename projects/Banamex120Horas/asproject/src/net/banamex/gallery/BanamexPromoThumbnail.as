package net.banamex.gallery
{
	import com.graphics.gallery.IThumbnail;
	import com.graphics.gallery.IThumbnailData;
	import com.graphics.gallery.Thumbnail;
	import com.graphics.gallery.ThumbnailEvent;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	import net.banamex.structures.BanamexPromoThumbnailData;
	
	public class BanamexPromoThumbnail extends Thumbnail implements IThumbnail {

		public static const ON_LINK_CLICK:String = "onLink";

		protected function get promoTxf():TextField {
			return (_background as MovieClip).back.promoTxf;
		}
		
		protected function get buyButton():SimpleButton {
			return (_background as MovieClip).back.buyOnlineButton;
		}

		protected function get finalText():MovieClip {
			return (_background as MovieClip).back.finalText;
		}

		override public function set data($value:IThumbnailData):void {
			super.data = $value;
			promoTxf.autoSize = TextFieldAutoSize.CENTER;
			promoTxf.wordWrap = true;
			promoTxf.multiline = true;
			promoTxf.text = ($value as BanamexPromoThumbnailData).promoText;
			finalText.y = promoTxf.y + promoTxf.textHeight;
		}

		public function BanamexPromoThumbnail($ID:String, $background:DisplayObject, $imageUrl:String="", $title:TextField=null) {
			super($ID, $background, $imageUrl, $title);
		}

		override protected function addContainer($add:Boolean = true):void {
			super.addContainer($add);
			addElement(_image, $add, (_background as MovieClip).container);
		}

		override protected function setInvisibleButton():void {}

		override protected function setSizeImage():void {
			super.setSizeImage();
			_image.scaleX = _image.scaleY = .8;
			_image.x = 2;
		}

		override protected function createThumbnailListeners($create:Boolean):void {
			super.createThumbnailListeners($create);
			addListener(buyButton, MouseEvent.CLICK, onClick, $create);
			((_background as MovieClip).container as MovieClip).useHandCursor = true;
			((_background as MovieClip).container as MovieClip).buttonMode = true;
			addListener((_background as MovieClip).container, MouseEvent.CLICK, onClick, $create);
		}

		protected function onClick($event:MouseEvent) :void {
			navigateToURL(new URLRequest((data as BanamexPromoThumbnailData).promoURL), '_blank');
			dispatchEvent(new ThumbnailEvent(_data.ID, ON_LINK_CLICK, _data, this));

		}
	}
}