package net.banamex.gallery {
	import com.graphics.gallery.IThumbnail;
	import com.graphics.gallery.Thumbnail;
	
	import flash.display.DisplayObject;
	import flash.text.TextField;
	
	public class BanamexElementFaceThumbnail extends Thumbnail implements IThumbnail {
		public function get image():DisplayObject{
			return _image;
		}
		
		public function BanamexElementFaceThumbnail($ID:String, $background:DisplayObject, $imageUrl:String="", $title:TextField=null) {
			super($ID, $background, $imageUrl, $title);
		}
	}
}