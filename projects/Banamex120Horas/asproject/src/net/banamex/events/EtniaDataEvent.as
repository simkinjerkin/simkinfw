package net.banamex.events {

	import flash.events.Event;

	public class EtniaDataEvent extends Event {

		protected var _data:Object;

		public function get data():Object {
			return _data;
		}

		public function EtniaDataEvent($data:Object, $type:String, $bubbles:Boolean = false, $cancelable:Boolean = false) {
			super($type, $bubbles, $cancelable);
			_data = $data;
		}
	}
}