package net.banamex {

	import com.core.config.SystemConfigurationManager;
	import com.core.config.URLManager;
	import com.core.system.System;
	import com.google.tracking.AnalyticManager;
	import com.graphics.gallery.ThumbnailData;
	import com.graphics.gallery.ThumbnailEvent;
	import com.graphics.gallery.ThumbnailEventType;
	import com.net.BasicLoaderEvent;
	import com.ui.AlertEvent;
	import com.ui.controllers.BasicStageController;
	import com.ui.controllers.SiteAlertManager;
	import com.ui.controllers.events.StageEvent;
	import com.ui.theme.SimpleButtonFactory;
	import com.utils.MathUtils;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GradientBevelFilter;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import net.GraphFBBasicMain;
	import net.banamex.controllers.BackgroundController;
	import net.banamex.controllers.FacebookRequestController;
	import net.banamex.controllers.GalleryController;
	import net.banamex.controllers.HomeController;
	import net.banamex.controllers.PicturePickerController;
	import net.banamex.controllers.RegisterController;
	import net.banamex.controllers.SWFPlayerController;
	import net.banamex.controllers.TermsController;
	import net.banamex.controllers.TransactionManager;
	import net.banamex.core.ContainerManager;
	import net.banamex.core.MovementsConfigManager;
	import net.banamex.structures.LoggedUserData;
	import net.banamex.structures.SWFSecuenceData;
	import net.core.FacebookGraph;
	import net.events.FacebookConnectLoginEvent;

	public class 120HoursMain extends GraphFBBasicMain {

		protected var _home:HomeController;
		protected var _background:BackgroundController;
		protected var _register:RegisterController;
		protected var _player:SWFPlayerController;
		protected var _picturePicker:PicturePickerController;
		protected var _gallery:GalleryController;
		protected var _loggedUserData:LoggedUserData;
		protected var _defaultCelebration:SWFSecuenceData;
		protected var _terms:TermsController;

		public function 120HoursMain($url:String="./swf/v1_0/config/localURLSData.xml") {
			super($url);
			SoundMixer.soundTransform = new SoundTransform(.05);
		}

		override protected function setPreloadMovies():void {
			System.addDebugMessage(System.loaderInfoRoot.parameters.videoID + " ::: " + System.loaderInfoRoot.parameters.bannerName);
			SiteAlertManager.getInstance().showLoader(true, "Cargando gráfico", System.stageRoot.stageWidth / 2, System.stageRoot.stageHeight / 2);
			_graphicsLoader.loadRequest("back", "Background.swf");
		}

		override protected function onClientReady():void {
			super.onClientReady();
			AnalyticManager.getInstance().setTracking(System.stageRoot, "UA-24285761-8", "AS3", !SystemConfigurationManager.getInstance().configurationData.enabledTracking);
			AnalyticManager.getInstance().setTrackPage('http://firmayganabnmx2012.com.mx/');
		}

		override protected function onPreloadedLoaded($event:Event):void {
			super.onPreloadedLoaded($event);
			_background = new BackgroundController(_graphicsLoader.library.back);
			addElement(_background.container, true, backgroundContainer);
		}

		override protected function loadGraphics():void {
			_graphicsLoader.loadRequest("home", "Home.swf");
			_graphicsLoader.loadRequest("register", "Register.swf");
			_graphicsLoader.loadRequest("picPicker", "PicturePicker.swf");
			_graphicsLoader.loadRequest("configurator", "Configurator.swf");
			_graphicsLoader.loadRequest("player", "Player.swf");
			_graphicsLoader.loadRequest("gallery", "Gallery.swf");
			_graphicsLoader.loadRequest("terms", "Terms.swf");
			_graphicsLoader.loadRequest("solo", "PictureSolo.swf");
			super.loadGraphics();
		}

		override protected function onGraphicsLoaded($event:Event):void {
			super.onGraphicsLoaded($event);
			SiteAlertManager.getInstance().showLoader(true, "Iniciando conexión a FB, recuerda desbloquear los pop-ups", System.stageRoot.stageWidth / 2, System.stageRoot.stageHeight / 2);
		}

		override protected function onLoginSuccess($event:FacebookConnectLoginEvent):void {
			super.onLoginSuccess($event);
			SiteAlertManager.getInstance().showLoader(false);
			_loggedUserData = new LoggedUserData(FacebookGraph.getInstance().userData.name, FacebookGraph.getInstance().userData.mail, FacebookGraph.getInstance().userData.gender);
			var transactor:TransactionManager = new TransactionManager();
			addListener(transactor, TransactionManager.ON_LOGIN_SUCCESS, onServerLoginSuccess);
			transactor.sendLogin();
		}

		protected function onServerLoginSuccess($event:BasicLoaderEvent):void {
			var transactor:TransactionManager = $event.currentTarget as TransactionManager;
			addListener(transactor, TransactionManager.ON_LOGIN_SUCCESS, onServerLoginSuccess, false);

			if($event.serverResponse.name && $event.serverResponse.name != "") {
				_loggedUserData = new LoggedUserData($event.serverResponse.name, $event.serverResponse.email, $event.serverResponse.gender, $event.serverResponse.gender != "");
			}

			updateCurrentName();
			
			addListener(transactor, TransactionManager.ON_MOVES_DATA_SUCCESS, onGetMovesData);
			transactor.getMovementConfig();
		}

		protected function updateCurrentName():void {
			ContainerManager.getInstance().currentName = System.loaderInfoRoot.parameters.bannerName ? System.loaderInfoRoot.parameters.bannerName : _loggedUserData.name;
		}

		protected function onGetMovesData($event:BasicLoaderEvent):void {
			var transactor:TransactionManager = $event.currentTarget as TransactionManager;
			addListener(transactor, TransactionManager.ON_MOVES_DATA_SUCCESS, onGetMovesData, false);

			var movs:Array = new Array();
			for(var i:uint = 0; i < $event.serverResponse.movements.length; i++) {
				var data:Object = $event.serverResponse.movements[i];
				var tData:ThumbnailData = new ThumbnailData(data.ID);
				tData.thumbnailURL = URLManager.getInstance().getPath("imagesDir") + data.thumbnailURL;
				tData.URL = URLManager.getInstance().getPath("videosDir") + data.URL;
				movs.push(tData);
			}

			MovementsConfigManager.movements = movs;
			_defaultCelebration = new SWFSecuenceData();
			_defaultCelebration.movements = new Array();

			_defaultCelebration.movements.push();

			if(System.loaderInfoRoot.parameters.videoID) {
				var videoData:SWFSecuenceData = new SWFSecuenceData();
				videoData.ID = System.loaderInfoRoot.parameters.videoID;
//				videoData.ID = "G2Q10150Y801U";
				showHome();
				showHome(false);
				showSWFPlayerController(videoData);
				_background.setRegisterView();
			} else {
				showHome();
			}
		}

		protected function showHome($add:Boolean = true):void {
			if(!_home) {
				_home = new HomeController(_graphicsLoader.library.home);
			}
			addListener(_home, BasicStageController.ON_STAGE_CHANGE, onHomeButtonClick, $add);
			addElement(_home.container, $add, mainContainer);
		}

		protected function onHomeButtonClick($event:StageEvent):void {
			if($event.currentStage != HomeController.ON_TERMS_BUTTON) {
				_background.setRegisterView();
				showHome(false);
			}
			switch($event.currentStage) {
				case HomeController.ON_CLIENT_BUTTON:					AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'SoyclienteBanamex');
																		showRegisterForm();							break;
				case HomeController.ON_NO_CLIENT_BUTTON:				AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'NoSoyclienteBanamex');
																		doNoClientVideo();							break;
				case HomeController.ON_GALLERY_BUTTON:					AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'VerCelebraciones');
																		onShowGalleryRequest();						break;
				case HomeController.ON_TERMS_BUTTON:					showTerms();								break;
			}
		}

		protected function showTerms($add:Boolean = true):void {
			if(!_terms) {
				_terms = new TermsController(_graphicsLoader.library.terms);
			}
			addElement(_terms.container, $add, panelContainer);
			addListener(_terms, TermsController.ON_CLOSE, onTermsClose, $add);
//			navigateToURL(new URLRequest(URLManager.getInstance().getPath("termsPageURL")), "_blank");
		}

		protected function onTermsClose($event:Event):void {
			showTerms(false);
			_home.unselectCurrentButton();
		}

		protected function doNoClientVideo($event:Event = null):void {
			if($event && $event.currentTarget == _gallery) {
				AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'GrabatuVideo');
				showGalleryController(false);
			}
			showSWFPlayerController(_defaultCelebration);
		}

		protected function showRegisterForm($add:Boolean = true):void {
			if(!_register) {
				_register = new RegisterController(_graphicsLoader.library.register);
			}
			_register.setDefaultData(_loggedUserData);
			addListener(_register, RegisterController.UNREGISTERED_VOUCHER, onRegisterRequest, $add);
			addListener(_register, RegisterController.ON_REGISTER_SUCCESS, onRegisterRequest, $add);
			addElement(_register.container, $add, mainContainer);
		}

		protected function onRegisterRequest($event:Event):void {
			if($event && $event.type == RegisterController.ON_REGISTER_SUCCESS) {
				AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'Participar');
				_loggedUserData.client = true;
			} else if($event && $event.type == RegisterController.UNREGISTERED_VOUCHER) {
				SiteAlertManager.getInstance().showAlert("Ayúdale al Chicharito a meter gol y festeja como él. Pero si no estás registrado, no participas por el premio de $10,000 pesos.", ["Aceptar", "Cancelar"], onUnregisteredAlertClose);
				return;
			}
			showRegisterForm(false);
			showSWFPlayerController();
		}

		protected function onUnregisteredAlertClose($event:AlertEvent):void {
			if($event.ID == 0) {
				AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'NoRegistrar');
				showRegisterForm(false);
				showSWFPlayerController();
			}
		}

		protected function showSWFPlayerController($data:SWFSecuenceData = null, $add:Boolean = true):void {
			if(!_player) {
				_player = new SWFPlayerController(_graphicsLoader.library.player);
				_player.container.x = 80;
				_player.container.y = 93;
			}
			_background.showReduce($add);
			addListener(_player, SWFPlayerController.ON_ASK_CLICK, onInfoRequest, $add);
			addListener(_player, SWFPlayerController.SHARE_REQUEST, onShareRequest, $add);
			addListener(_player, SWFPlayerController.SHOW_GALLERY_REQUEST, onShowGalleryRequest, $add);
			addListener(_player, SWFPlayerController.BACK_CLICK, onShowGalleryRequest, $add);
			addListener(_player, SWFPlayerController.ON_PERSONALIZE_CLICK, onPersonalizeButtonClick, $add);
			addListener(_player, SWFPlayerController.ON_VIDEO_COMPLETE, onPersonalizeButtonClick, $add);
			addElement(_player.container, $add, mainContainer);
			if($add) {
				_player.setSequenceData($data ? $data : _defaultCelebration);
				ContainerManager.getInstance().currentName = ($data && $data.owner && $data.owner != "" ? $data.owner : _loggedUserData.name);
			} else {
				_player.stop();
			}
		}

		protected function onInfoRequest($event:Event):void {
			AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'SolicitalaAqui');
			navigateToURL(new URLRequest(URLManager.getInstance().getPath("askCardPageURL")), "_blank");
		}

		protected function onShareRequest($event:Event):void {
			var fbTransactor:FacebookRequestController = new FacebookRequestController();
			fbTransactor.post();
		}

		protected function onShowGalleryRequest($event:Event = null):void {
			if($event && $event.currentTarget == _player) {
				showSWFPlayerController(null, false);
//				if(_gallery && $event.type == SWFPlayerController.BACK_CLICK) {
//					_gallery.refreshData();
//				}
			}
			if($event && $event.type == SWFPlayerController.BACK_CLICK && !_gallery) {
				_background.setInitView();
				showHome();
			} else {
				showGalleryController();
			}
		}

		protected function showGalleryController($add:Boolean = true):void {
			if(!_gallery) {
				_gallery = new GalleryController(_graphicsLoader.library.gallery);
			}
			if($add) {
				_gallery.refreshData(1);
			} else {
				_gallery.destructor();
			}
			addListener(_gallery, ThumbnailEventType.ON_PRESS, onPlayerRequest, $add);
			addListener(_gallery, GalleryController.DO_VIDEO_REQUEST, doNoClientVideo, $add);
			addListener(_gallery, GalleryController.SHARE_REQUEST, onShareRequest, $add);
			addElement(_gallery.container, $add, mainContainer);
		}

		protected function onPlayerRequest($event:ThumbnailEvent):void {
			if(!($event.data as SWFSecuenceData).movements || !($event.data as SWFSecuenceData).movements[0]) {
				SiteAlertManager.getInstance().showAlert("Datos corruptos", ["OK"], onAlertClose);
				return;
			}
			showGalleryController(false);
			showSWFPlayerController($event.data as SWFSecuenceData);
		}

		protected function onAlertClose($event:AlertEvent):void {}

		protected function showPicturePicker($add:Boolean = true):void {
			if(!_picturePicker) {
				_picturePicker = new PicturePickerController(_graphicsLoader.library.picPicker);
				_picturePicker.confGraphic = _graphicsLoader.library.configurator;
				_picturePicker.soloGraphic = _graphicsLoader.library.solo;
			}
			if($add) {
				_picturePicker.reinit();
			}
			panelContainer
			addListener(_picturePicker, PicturePickerController.ON_CELEBRATION_MADE, onCelebrationMade, $add);
			addListener(_picturePicker, PicturePickerController.ON_HOME_CLICK, onHomeClick, $add);
			addElement(_picturePicker.container, $add, mainContainer);
		}

		protected function onCelebrationMade($event:Event):void {
			showPicturePicker(false);
			showSWFPlayerController(_picturePicker.sequenceData);
			_player.showFinalPanel(_loggedUserData.client);
		}

		protected function onHomeClick($event:Event):void {
			showPicturePicker(false);
			showSWFPlayerController();
		}

		protected function onPersonalizeButtonClick($event:Event):void {
			if($event && $event.type == SWFPlayerController.ON_PERSONALIZE_CLICK) {
				AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'PersonalizarVideo');
			}
			SiteAlertManager.getInstance().showLoader(false);
			updateCurrentName();
			showSWFPlayerController(null, false);
			showPicturePicker();
		}

	}
}