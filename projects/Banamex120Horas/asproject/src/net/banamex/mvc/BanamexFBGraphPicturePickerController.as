package net.banamex.mvc {

	import com.facebook.graph.Facebook;
	import com.graphics.gallery.ThumbnailEvent;
	import com.ui.controllers.mvc.fbpicture.BasicFBPicturePickerStates;
	import com.ui.controllers.mvc.interfaces.IFormController;
	import com.ui.controllers.mvc.interfaces.IModel;
	
	import net.core.FacebookGraph;
	import net.structures.PhotoData;
	import net.ui.mvc.fbpicture.BasicFBGraphPicturePickerController;

	public class BanamexFBGraphPicturePickerController extends BasicFBGraphPicturePickerController implements IFormController {

		protected function get bnxModel():BanamexFBGPhotoPickerModel {
			return _model as BanamexFBGPhotoPickerModel;
		}

		public function BanamexFBGraphPicturePickerController($model:IModel) {
			super($model);
		}

		override public function clickHandler($ID:String):void {
			var delta:int = 1;
			switch($ID) {
				case BasicFBPicturePickerStates.MY_ALBUMS_BTN:					_model.setState(BasicFBPicturePickerStates.INIT);
																				getUserPhotos();
																				return;
				case BanamexFBPicturePickerStates.FB_GALLERY_BACK_BUTTON:		delta = -1;
				case BanamexFBPicturePickerStates.FB_GALLERY_NEXT_BUTTON:		bnxModel.photosOffset = Math.max(0, bnxModel.photosOffset + (bnxModel.photosLimit * delta));
																				getUserPhotos();
			}
			super.clickHandler($ID);
		}

		override public function setSelectedPhoto($event:ThumbnailEvent):void {
			super.setSelectedPhoto($event);
			clickHandler(BasicFBPicturePickerStates.SELECT_BTN);
		}

		public function getUserPhotos($userID:String = "me"):void {
			if(_model.currentState != BasicFBPicturePickerStates.LOADING_ALBUMS) {
				fbPPModel.uid = FacebookGraph.getInstance().userData.ID;
				Facebook.api("/" + $userID + "/photos", handleUserPhotosDataLoaded, {type:"square", limit:bnxModel.photosLimit, offset:bnxModel.photosOffset});
				_model.setState(BasicFBPicturePickerStates.PHOTOS_LOADING);
			}
		}

		protected function handleUserPhotosDataLoaded($result:Object, $fail:Object):void {
			if($result) {
				var photosData:Array = new Array();
				for(var i:uint = 0; i < $result.length; i++) {
					photosData.push(new PhotoData($result[i]));
				}
				fbPPModel.photos = photosData;
			} else {
				_model.setError("Error al pedir las fotos del álbum seleccionado");
			}
		}

	}
}