package net.banamex.mvc {

	import com.ui.controllers.mvc.fbpicture.IPhotoPicker;
	
	import net.ui.mvc.fbpicture.FacebookGraphPhotoPickerModel;

	public class BanamexFBGPhotoPickerModel extends FacebookGraphPhotoPickerModel implements IPhotoPicker {

		protected var _photosOffset:uint = 0;
		protected var _totalPhotos:uint = 0;
		protected var _photosLimit:uint = 10;

		public function get totalPhotos():uint {
			return _totalPhotos;
		}
		
		public function set totalPhotos($value:uint):void {
			_totalPhotos = $value;
		}

		public function get photosLimit():uint {
			return _photosLimit;
		}
		
		public function set photosLimit($value:uint):void {
			_photosLimit = $value;
		}

		public function get photosOffset():uint {
			return _photosOffset;
		}

		public function set photosOffset($value:uint):void {
			_photosOffset = $value;
		}

		public function BanamexFBGPhotoPickerModel() {
			super();
		}

	}

}