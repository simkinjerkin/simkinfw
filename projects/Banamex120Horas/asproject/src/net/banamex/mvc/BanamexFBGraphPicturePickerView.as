package net.banamex.mvc {

	import com.core.system.System;
	import com.graphics.gallery.SimpleGallery;
	import com.graphics.gallery.Thumbnail;
	import com.graphics.gallery.ThumbnailEvent;
	import com.ui.SiteAlert;
	import com.ui.controllers.SiteAlertManager;
	import com.ui.controllers.mvc.fbpicture.BasicFBPicturePickerStates;
	import com.ui.controllers.mvc.interfaces.IController;
	import com.ui.controllers.mvc.interfaces.IModel;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import net.graphics.PhotoThumbnail;
	import net.structures.PhotoData;
	import net.ui.mvc.fbpicture.BasicFBGraphPicturePickerView;

	public class BanamexFBGraphPicturePickerView extends BasicFBGraphPicturePickerView {

		protected function get fbMenu():MovieClip {
			return graphicMC.fbMenu;
		}

		protected function get backGraphic():SimpleButton {
			return fbMenu.backButtonGraphic;
		}

		protected function get nextGraphic():SimpleButton {
			return fbMenu.nextButtonGraphic;
		}

		protected function get backButton():SimpleButton {
			return fbMenu.fbBackButton;
		}

		protected function get nextButton():SimpleButton {
			return fbMenu.fbNextButton;
		}

		protected function get panelContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("panelContainer");
		}

		protected function get bnxModel():BanamexFBGPhotoPickerModel {
			return _model as BanamexFBGPhotoPickerModel;
		}

		public function BanamexFBGraphPicturePickerView($model:IModel, $controller:IController=null, $graphic:Sprite=null) {
			super($model, $controller, $graphic);
		}

		override protected function initialize():void {
			super.initialize();
			formController.clickHandler(BasicFBPicturePickerStates.MY_ALBUMS_BTN);
			addElement(fbMenu, true, panelContainer);
			addButtons(false, false);
			SiteAlertManager.getInstance().showLoader(true, "Obteniendo información de Facebook", System.stageRoot.stageWidth / 2, (System.stageRoot.stageHeight / 2) + 75, true);
		}

		override public function destructor():void {
			addElement(fbMenu, false, panelContainer);
			super.destructor();
		}

		override protected function addGallery($data:Array):void {
			super.addGallery($data);
			addButtons(bnxModel.photosOffset != 0, $data.length == bnxModel.photosLimit);
		}

		override protected function onGalleryRender($event:Event):void {
			super.onGalleryRender($event);
			SiteAlertManager.getInstance().showLoader(false);
		}

		override protected function createGallery():SimpleGallery {
			if(!_gallery) {
				_gallery = new SimpleGallery(3, -1);
				_gallery.paddingX = 15;
				_gallery.paddingY = 15;
				_gallery.x = 90;
				_gallery.y = 310;
				_gallery.adjustImage = true;
				_gallery.thumbClass = PhotoThumbnail;
				_gallery.backgroundClass = SWCSmallThumbnail;
				_gallery.mask = getMask();
			}
			_gallery.alpha = 1;
			return _gallery;
		}

		override protected function initGraphic():void {}

		override protected function getButtonID($button:InteractiveObject):String {
			switch($button) {
				case backButton:		return BanamexFBPicturePickerStates.FB_GALLERY_BACK_BUTTON;
				case nextButton:		return BanamexFBPicturePickerStates.FB_GALLERY_NEXT_BUTTON;
			}
			return super.getButtonID($button);
		}

		override protected function stateUpdate($event:Event):void {
			super.stateUpdate($event);
			trace("stateUpdate :: " + bnxModel.currentState);
			if(bnxModel.currentState == BasicFBPicturePickerStates.INIT) {
				SiteAlertManager.getInstance().showLoader(true, "Obteniendo información de Facebook", System.stageRoot.stageWidth / 2, (System.stageRoot.stageHeight / 2) + 75, true);
			}
			if(bnxModel.currentState == BasicFBPicturePickerStates.PHOTOS_LOADING) {
				addButtons(false, false);
				SiteAlertManager.getInstance().showLoader(true, "Obteniendo información de Facebook", System.stageRoot.stageWidth / 2, (System.stageRoot.stageHeight / 2) + 75, true);
				if(_gallery) {
					_gallery.alpha = .7;
				}
			}
			if(bnxModel.currentState == BasicFBPicturePickerStates.PHOTOS_DATA_LOADED) {
				trace("afsd");
//				SiteAlertManager.getInstance().showLoader(false);
			}
		}

		override protected function onGalleryThumbPress($event:ThumbnailEvent):void {
			if(bnxModel.currentState == BasicFBPicturePickerStates.PHOTOS_LOADING) {
				return;
			}
			super.onGalleryThumbPress($event);
		}

		protected function addButtons($back:Boolean = true, $next:Boolean = true):void {
			backGraphic.alpha = $back ? 1 : .5;
			nextGraphic.alpha = $next ? 1 : .5;
			addElement(backButton, $back, fbMenu);
			addElement(nextButton, $next, fbMenu);
			addListener(backButton, MouseEvent.CLICK, clickHandler, $back);
			addListener(nextButton, MouseEvent.CLICK, clickHandler, $next);
		}

		protected function onBackNextButtonClick($event:MouseEvent):void {
			addButtons(false, false);
		}
		
		protected function getMask():Sprite {
			var mask:Sprite = new Sprite();
			mask.graphics.beginFill(0, .5);
			mask.graphics.drawRect(90, 310, 300, 300);
			return mask;
		}
	}
}