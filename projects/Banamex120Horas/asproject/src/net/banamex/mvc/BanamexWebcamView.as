package net.banamex.mvc {

	import com.ui.controllers.mvc.interfaces.IController;
	import com.ui.controllers.mvc.interfaces.IModel;
	import com.ui.controllers.mvc.userpicture.views.StandardWebcamView;
	
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;

	public class BanamexWebcamView extends StandardWebcamView {

		override protected function get backButton():InteractiveObject {
			return null;
		}

		override protected function get finishButton():InteractiveObject {
			return null;
		}

		override protected function get cancelButton():InteractiveObject {
			return null;
		}

		override protected function get captureImageButton():InteractiveObject {
			return null;
		}

		override protected function get privacyButton():InteractiveObject {
			return null;
		}

		override protected function get webcamConfButton():InteractiveObject {
			return null;
		}

		override protected function get counter():MovieClip {
			return null;
		}

		override protected function get backText():InteractiveObject {
			return null;
		}

		public function BanamexWebcamView($model:IModel, $controller:IController=null, $graphic:Sprite=null) {
			super($model, $controller, $graphic);
		}

	}
}