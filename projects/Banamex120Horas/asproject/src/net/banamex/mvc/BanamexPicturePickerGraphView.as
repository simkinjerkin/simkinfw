package net.banamex.mvc {

	import com.core.system.System;
	import com.ui.controllers.SiteAlertManager;
	import com.ui.controllers.mvc.interfaces.IController;
	import com.ui.controllers.mvc.interfaces.IModel;
	import com.ui.controllers.mvc.userpicture.UserPicturePickerStates;
	import com.ui.controllers.mvc.userpicture.views.StandardWebcamView;
	
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	import net.ui.mvc.userpicture.views.StandardPicturePickerGraphView;

	public class BanamexPicturePickerGraphView extends StandardPicturePickerGraphView {

		override protected function get fbPictureButton():InteractiveObject {
			return homeGraphic.facebook;
		}

		override protected function get webCamPicButton():InteractiveObject {
			return homeGraphic.webcam;
		}

		override protected function get userFilePicButton():InteractiveObject {
			return null;
		}

		override protected function get cancelButton():InteractiveObject {
			return null;
		}

		protected function get homeGraphic():MovieClip {
			return graphicMC.homeGraphic;
		}

		public function BanamexPicturePickerGraphView($model:IModel, $controller:IController = null, $graphic:Sprite = null, $graphics:Dictionary = null) {
			super($model, $controller, $graphic, $graphics);
		}

		public function forceFBView():void {
			formController.clickHandler(UserPicturePickerStates.FB_PICTURE_MODE);
		}

		override protected function setFBPicPickerView():void {
			addView(new BanamexFBGraphPicturePickerView(userPickerModel.fbPicModel,
				userPickerModel.selectFileController, graphicMC.fbGraphic));
		}

		override protected function setWebcamPictureView():void {
			var view:BanamexWebcamView = new BanamexWebcamView(userPickerModel.webcamPicModel,
				userPickerModel.selectFileController, _graphics.fbGraphic);
				view.webCamContainerPosition = _webCamContainerPosition;
			view.init();
			addView(view);
		}

		override protected function showLoader($add:Boolean = true):void {
			SiteAlertManager.getInstance().showLoader($add, _model.currentState == UserPicturePickerStates.FB_PICTURE_MODE ? "Obteniendo información de Facebook" : "Cargando imagen elegida", System.stageRoot.stageWidth / 2, (System.stageRoot.stageHeight / 2) + 75, true);
		}

	}
}