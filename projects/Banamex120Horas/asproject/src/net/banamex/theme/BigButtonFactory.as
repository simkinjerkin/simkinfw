package net.banamex.theme {

	import com.ui.theme.SimpleButtonFactory;
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.text.TextFieldAutoSize;

	public class BigButtonFactory {

		static public function getButton($text:String, $x:Number = 0, $y:Number = 0):SimpleButton {
			return SimpleButtonFactory.getButton($text, $x, $y);
			var overButton:MovieClip = setButtonSize(new SWCBigButtonOver(), $text);
			var hitButton:MovieClip = new SWCBigButtonHit();
			hitButton.width = overButton.width;
			var button:SimpleButton = new SimpleButton(setButtonSize(new SWCBigButtonUp(), $text), overButton,
				setButtonSize(new SWCBigButtonDown(), $text), hitButton);
			button.x = $x;
			button.y = $y;
			return button;
		}

		static protected function setButtonSize($button:MovieClip, $text:String):MovieClip {
			var currentButton:MovieClip = $button;
			currentButton._txt.multiline = false;
			currentButton._txt.wordWrap = false;
			currentButton._txt.autoSize = TextFieldAutoSize.CENTER;
			currentButton._txt.text = $text;
			if(currentButton.back && currentButton._txt.width > currentButton.back.width) {
				currentButton.back.width = currentButton._txt.width + 20;
				currentButton._txt.x = (currentButton.back.width - currentButton._txt.width) / 2;
			}
			return currentButton;
		}

	}
}