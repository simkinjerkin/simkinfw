package net.banamex.controllers {

	import com.core.system.System;
	import com.graphics.gallery.SimpleGallery;
	import com.graphics.gallery.ThumbnailData;
	import com.graphics.gallery.ThumbnailEvent;
	import com.graphics.gallery.ThumbnailEventType;
	import com.net.BasicLoaderEvent;
	import com.ui.controllers.BasicController;
	import com.ui.controllers.SiteAlertManager;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import flashx.textLayout.events.UpdateCompleteEvent;
	
	import net.banamex.core.MovementsConfigManager;
	import net.banamex.graphics.CelebrationThumbnail;
	import net.banamex.structures.SWFSecuenceData;
	import net.core.FacebookGraph;

	public class GalleryController extends BasicController {

		public static const SHARE_REQUEST:String = "shareReq";
		public static const DO_VIDEO_REQUEST:String = "doVideoReq";

		public static const DATE:String = "date";
		public static const VOTES:String = "votes";

		protected var _currentPage:uint = 1;
		protected var _currentOrder:String = DATE;
		protected var _totalPages:uint = 10;
		protected var _gallery:SimpleGallery;

		protected function get pagerPanel():MovieClip {
			return containerMC.pagerPanel;
		}

		protected function get comboButton():SimpleButton {
			return containerMC.comboButton;
		}

		protected function get backButton():SimpleButton {
			return pagerPanel.backButton;
		}

		protected function get nextButton():SimpleButton {
			return pagerPanel.nextButton;
		}

		protected function get doVideoButton():SimpleButton {
			return containerMC.doVideoButton;
		}

		protected function get shareButton():SimpleButton {
			return containerMC.shareButton;
		}

		protected function get panelContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("panelContainer");
		}

		protected function get comboMenu():MovieClip {
			return containerMC.comboMenu;
		}

		protected function get recentButton():SimpleButton {
			return comboMenu.recent;
		}

		protected function get votedButton():SimpleButton {
			return comboMenu.voted;
		}

		protected function get orderTxf():TextField {
			return containerMC.orderTxf;
		}

		public function GalleryController($container:Sprite) {
			super($container);
		}

		public function destructor():void {
			addElement(pagerPanel, false, panelContainer);
			showComboMenu(false);
		}

		public function refreshData($page:uint = 1):void {
			addElement(_gallery, false);
			getData($page);
		}

		override protected function initialize():void {
			super.initialize();
//			getData();
			addArrowButtons(false, false);
			addButtonListeners();
			addElement(pagerPanel, true, panelContainer);
			addElement(comboMenu, false);
		}

		protected function addButtonListeners($add:Boolean = true):void {
			addListener(comboButton, MouseEvent.CLICK, onButtonClick, $add);
			addListener(doVideoButton, MouseEvent.CLICK, onButtonClick, $add);
			addListener(shareButton, MouseEvent.CLICK, onButtonClick, $add);
		}

		protected function showPager($add:Boolean = true, $currentValue:uint = 5, $pages:uint = 10):void {
			addArrowButtons($currentValue != 1, $currentValue != $pages);
			addPagerLabels($add, $currentValue, $pages);
		}

		protected var _labels:Array;

		protected function addPagerLabels($add:Boolean = true, $currentValue:uint = 5, $maxValue:uint = 10):void {
			trace("addPagerLabels ::: " + arguments);
			if(!_labels) {
				_labels = new Array();
				for(var j:uint = 0; j < 7; j++) {
					var label:TextField = ((new SWCPagerLabel()) as MovieClip)._txf;
					label.x = 167 + (28 * j);
					label.y = backButton.y;
					_labels.push(label);
					
				}
			}
			for(var i:uint = 0; i < 7; i++) {
				addElement(_labels[i], $add, pagerPanel);
			}
			if($add) {
				trace($currentValue);
				if($maxValue < 8) {
					setNumericLabels($currentValue, $maxValue);
				} else {
					getLabel(0).text = "1";
					if($currentValue < 5) {
						getLabel(1).text = "2";
						getLabel(2).text = "3";
						getLabel(3).text = "4";
						getLabel(4).text = "5";
						getLabel(5).text = "...";
						getLabel(6).text = $maxValue.toString();
					} else if($currentValue > $maxValue - 4) {
						trace("Entra al final");
						getLabel(1).text = "...";
						getLabel(2).text = ($maxValue - 4).toString();
						getLabel(3).text = ($maxValue - 3).toString();
						getLabel(4).text = ($maxValue - 2).toString();
						getLabel(5).text = ($maxValue - 1).toString();
						getLabel(6).text = $maxValue.toString();
					} else {
						getLabel(1).text = "...";
						getLabel(2).text = ($currentValue - 1).toString();
						getLabel(3).text = $currentValue.toString();
						getLabel(4).text = ($currentValue + 1).toString();
						getLabel(5).text = "...";
						getLabel(6).text = $maxValue.toString();
					}
					formatLabels($currentValue);
				}
			}

		}

		protected function setNumericLabels($current:uint = 1, $maxValue:uint = 7):void {
			for(var i:uint = 0; i < 7; i++) {
				if(i < $maxValue) {
					getLabel(i).text = (i + 1).toString();
				} else {
					getLabel(i).text = "";
				}
				getLabel(i).setTextFormat(new TextFormat(null, null, (i + 1) == $current ? 0x234234 : 0xFFFFFF));
			}
		}

		protected function formatLabels($current:uint = 1):void {
			for(var i:uint = 0; i < 7; i++) {
				if(getLabel(i).text == $current.toString()) {
					getLabel(i).setTextFormat(new TextFormat(null, null, 0x234234));
				} else {
					getLabel(i).setTextFormat(new TextFormat(null, null, 0xFFFFFF));
				}
			}
		}

		protected function getLabel($value:uint):TextField {
			return _labels[$value];
		}

		protected function addArrowButtons($left:Boolean = true, $right:Boolean = true):void {
			addListener(backButton, MouseEvent.CLICK, onButtonClick, $left);
			addElement(backButton, $left, pagerPanel);
			addListener(nextButton, MouseEvent.CLICK, onButtonClick, $right);
			addElement(nextButton, $right, pagerPanel);
		}

		protected function onButtonClick($event:MouseEvent):void {
			var dir:int = 1;
			switch($event.currentTarget) {
				case comboButton:				doOnComboClick();											break;
				case doVideoButton:				dispatchEvent(new Event(DO_VIDEO_REQUEST));					break;
				case shareButton:				dispatchEvent(new Event(SHARE_REQUEST));					break;
				case backButton:				dir = -1;
				case nextButton:				refreshData(_currentPage + dir);
												break;
			}
		}

		protected function doOnComboClick():void {
			showComboMenu();
		}

		protected function showComboMenu($add:Boolean = true):void {
			addListener(recentButton, MouseEvent.CLICK, onComboButton, $add);
			addListener(votedButton, MouseEvent.CLICK, onComboButton, $add);
			addElement(comboMenu, $add);
		}

		protected function onComboButton($event:MouseEvent):void {
			showComboMenu(false);
			if($event.currentTarget == recentButton) {
				if(_currentOrder == DATE) {
					return;
				}
				_currentOrder = DATE;
				orderTxf.text = "Más recientes";
			} else {
				if(_currentOrder == VOTES) {
					return;
				}
				_currentOrder = VOTES;
				orderTxf.text = "Más votados";
			}
			_currentPage = 1;
			refreshData(_currentPage);
		}

		protected function getData($page:uint = 1):void {
			addElement(_gallery, false);
			var transactor:TransactionManager = new TransactionManager();
			addListener(transactor, TransactionManager.ON_GALLERY_DATA_SUCCESS, onGetData);
			transactor.getGalleryData($page, _currentOrder);
		}

		protected function onGetData($event:BasicLoaderEvent):void {
			var dataArr:Array = new Array();
			_currentPage = $event.serverResponse.pag;
			_totalPages = $event.serverResponse.total_videos;
			for(var i:uint = 0; i < $event.serverResponse.videos.length; i++) {
				var data:SWFSecuenceData = new SWFSecuenceData($event.serverResponse.videos[i]);
				dataArr.push(data);
			}
			createGallery(dataArr);
			showPager(true, _currentPage, Math.ceil(_totalPages / 8));
			addElement(pagerPanel, true, panelContainer);
			SiteAlertManager.getInstance().showLoader(true, "Obteniendo imágenes", System.stageRoot.stageWidth / 2, System.stageRoot.stageHeight / 2, true);
		}

		protected function createGallery($data:Array):void {
			trace("createGallery");
			if(!_gallery) {
				_gallery = new SimpleGallery(-1, 4);
				_gallery.backgroundClass = SWCCelebrationThumbnail;
				_gallery.thumbClass = CelebrationThumbnail;
				_gallery.adjustImage = true;
				_gallery.paddingX = 5;
				_gallery.paddingY = 8;
				_gallery.x = 162;
				_gallery.y = 220;
				addListener(_gallery, ThumbnailEventType.ON_PRESS, onThumbPress);
			}
			addListener(_gallery, SimpleGallery.ON_GALLERY_RENDER, onGalleryRender);
			_gallery.initializeGallery($data.slice(0,8));
		}

		protected function onGalleryRender($event:Event):void {
			trace("onGalleryRender");
			SiteAlertManager.getInstance().showLoader(false);
			addListener(_gallery, SimpleGallery.ON_GALLERY_RENDER, onGalleryRender, false);
			addElement(_gallery);
		}

		protected function onThumbPress($event:ThumbnailEvent):void {
			dispatchEvent(new ThumbnailEvent($event.ID, $event.type, $event.data, $event.thumb, $event.bubbles, $event.cancelable));
		}

	}
}