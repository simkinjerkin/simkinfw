package net.banamex.controllers {
	import caurina.transitions.Tweener;
	
	import com.core.config.URLManager;
	import com.core.system.System;
	import com.facebook.graph.Facebook;
	import com.google.tracking.AnalyticManager;
	import com.graphics.gallery.ThumbnailEvent;
	import com.graphics.gallery.ThumbnailEventType;
	import com.net.BasicLoaderEvent;
	import com.net.GraphicLoadManager;
	import com.net.GraphicLoadManagerEventType;
	import com.ui.controllers.BasicController;
	import com.ui.theme.SimpleButtonFactory;
	import com.utils.MathUtils;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.LocalConnection;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	
	import net.banamex.gallery.BanamexPromoThumbnail;
	import net.banamex.structures.BanamexPromoThumbnailData;

	public class Banamex120HoursHomeController extends BasicController {

		public static const ON_CONTINUE_CLICK:String = "on continue click";

		protected var _doIt:SimpleButton;
		protected var _servicesCtrl:Banamex120HoursServicesController;
		protected var _graphicLoader:GraphicLoadManager;
		protected var _dataArray:Array;
		protected var _currentPromo:uint = 0;
		protected var _promoThumb:BanamexPromoThumbnail;
		protected var _isNextButton:Boolean= true;
		protected var _lastThumb:BanamexPromoThumbnail;
		protected var _serviceTestMode:Boolean = false;
		protected var _screenGraphic:MovieClip;
		
		public function get randomPromotion():BanamexPromoThumbnailData {
			if(_dataArray) {
				return _dataArray[MathUtils.randomRange(0, _dataArray.length - 1)];
			}
			return new BanamexPromoThumbnailData("", "", {});
		}

		protected function get promoMc():MovieClip {
			return containerMC.promoMc;
		}
		
		protected function get rightButton():SimpleButton {
			return  promoMc.rightButton;
		}
		
		protected function get leftButton():SimpleButton {
			return  promoMc.leftButton;
		}
		
		public function Banamex120HoursHomeController($container:Sprite, $serviceTestMode:Boolean) {
			super($container);
			addButtons();
			geteMaskThumbnailPromo();
			getUserLikedPages();
			hideArrowsButton();
			addArrowButtons();
		}

		public function loadImage():void {
			if(!_graphicLoader) {
				_graphicLoader = new GraphicLoadManager();
				_graphicLoader.graphicsDirPath = "";
			}
			_graphicLoader.loadRequest("userImg", URLManager.getInstance().getPath("serviceDir") + "shot/" + System.loaderInfoRoot.parameters.ID);
			addListener(_graphicLoader, GraphicLoadManagerEventType.ON_GRAPHICS_LOADED, onGraphicsLoad);
		}

		protected function onGraphicsLoad($event:GraphicLoadManagerEventType):void {
			addListener(_graphicLoader, GraphicLoadManagerEventType.ON_GRAPHICS_LOADED, onGraphicsLoad, false);
			addImage(_graphicLoader.library.userImg);
		}

		protected function addImage($userImage:DisplayObject):void {
			addElement($userImage, true, _screenGraphic.photoContainer);
		}
		
		protected function getUserLikedPages():void {
			Facebook.api("me/likes", userLikedPagesResponse, {locale:"es_LA"});
		}
		
		private function userLikedPagesResponse($result:Object, $fail:Object):void {
			var string:String;
			if($result) {
				string = getArrayString(getOrderedArray(getCategoriesCount($result as Array)));
			} else {
				string = _serviceTestMode && (new LocalConnection()).domain == "localhost" ? "Automóviles, Página de aplicación, Compañía, Comunidad, Internet/software, Negocio local, Servicios profesionales, Bar" : "";
			}
			loadInfo(string);
		}

		private function getCategoriesCount($data:Array):Dictionary {
			var data:Dictionary = new Dictionary();
			for(var i:uint = 0; i < $data.length; i++) {
				data[$data[i].category] = !data[$data[i].category] ? 1 : data[$data[i].category] + 1;
			}
			return data;
		}
		
		private function getOrderedArray($data:Dictionary):Array {
			var data:Array = new Array();
			for(var ID:String in $data) {
				data.push({"ID":ID, "likes":$data[ID]});
			}
			data.sortOn("likes", Array.NUMERIC | Array.DESCENDING);
			return data;
		}

		private function getArrayString($data:Array):String {
			var str:String = "";
			for(var i:uint = 0; i < $data.length; i++) {
				str = str + $data[i].ID + (i != $data.length - 1 ? ", " : "");
			}
			return str;
		}

		protected function loadInfo($categories:String):void {
			var transactor:Banamex120HoursServicesController = new Banamex120HoursServicesController(null);
			addListener(transactor, Banamex120HoursServicesController.ON_COMPLETE, onComplete);
			transactor.getPromos($categories);
		}

		protected function onComplete($event:BasicLoaderEvent):void {
			addListener(_servicesCtrl, Banamex120HoursServicesController.ON_COMPLETE, onComplete, false);
			var data:Array = $event.serverResponse.data;
			_dataArray = [];
			for(var str:String in data){
				var thumb:BanamexPromoThumbnailData = new BanamexPromoThumbnailData(data[str].id, data[str].name, data[str]);
				_dataArray.push(thumb);	
			}
			createThumbnail();
		}
		
		protected function createThumbnail($index:uint = 0):void{
			hideArrowsButton();
			if(_promoThumb){
				addListeners(false);
				_promoThumb = null;
			}
			_promoThumb = new BanamexPromoThumbnail($index.toString(), new SWCPromoThumbnail());
			_promoThumb.data = _dataArray[$index];
			addListener(_promoThumb, ThumbnailEventType.ON_THUMBNAIL_READY, onThumbReady);
		}
		
		protected function onThumbReady($event:Event):void {
			_promoThumb.x = (_isNextButton) ? 500 : -500;
			addElement(_promoThumb, true, promoMc);
			Tweener.addTween(_lastThumb, {x:(_isNextButton) ? -500 : 500, time:1.5});
			Tweener.addTween(_promoThumb, {x:-3, time:1.5,  onComplete:onTweenComplete});
			addListeners();
		}

		protected function onLinkClick($event:ThumbnailEvent):void {
			AnalyticManager.getInstance().setTrackEvent('promos', 'click', 'socio_' + ($event.data as BanamexPromoThumbnailData).providerID);
		}

		protected function addListeners($add:Boolean = true):void {
			addListener(_promoThumb, BanamexPromoThumbnail.ON_LINK_CLICK, onLinkClick, $add);
			addListener(leftButton, MouseEvent.CLICK, onArrowsClick, $add);
			addListener(rightButton, MouseEvent.CLICK, onArrowsClick, $add);
		}
		
		protected function addButtons($show:Boolean = true):void {
			if(!_doIt) {
				_doIt = SimpleButtonFactory.getButton("Haz tu foto", 150, 550);
			}
			addListener(_doIt, MouseEvent.CLICK, onClick);
			addElement(_doIt, $show, containerMC);
		}

		protected function onArrowsClick($event:MouseEvent):void {
			AnalyticManager.getInstance().setTrackEvent('promos', 'click', $event.currentTarget == leftButton ? 'left' : 'right');
			if($event.currentTarget == leftButton) {
				if (_currentPromo > 0) {
					_isNextButton = false;
					_lastThumb = _promoThumb;
					createThumbnail(--_currentPromo);
				}
			}else{
				if ((_currentPromo + 1) < _dataArray.length) {
					_lastThumb = _promoThumb;
					_isNextButton = true;
					createThumbnail(++_currentPromo);
				}
			}
		}
		
		protected function onClick($event:MouseEvent):void {
			addArrowButtons(false);
			switch($event.currentTarget){
				case _doIt:			AnalyticManager.getInstance().setTrackEvent('home', 'click', 'hazTuFoto');
									dispatchEvent(new Event(ON_CONTINUE_CLICK));	break;
			}
		}
		
		
		public function hideArrowsButton($hide:Boolean = true):void {
			leftButton.visible = !$hide;
			rightButton.visible = !$hide;
		}
		
		public function addArrowButtons($add:Boolean = true):void {
			rightButton.x += 75;
			leftButton.x += 75;
			leftButton.y = rightButton.y = 440;
			addElement(leftButton, $add, panelContainer);
			addElement(rightButton, $add, panelContainer);
		}
		
		protected function onTweenComplete():void {
			hideArrowsButton(false);
			validateArrows();
		}
		
		protected function validateArrows():void {
			if(_currentPromo == 0){
				leftButton.visible = false;
				rightButton.visible = true;
			}else if(_currentPromo > 0 && _currentPromo < _dataArray.length - 1){
				hideArrowsButton(false);
			}else if(_currentPromo <= _dataArray.length - 1){
				leftButton.visible = true;
				rightButton.visible = false;
			}
		}
		
		protected function get panelContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("panelContainer");
		}
		protected function geteMaskThumbnailPromo():void {
			var mask:Sprite = new Sprite();
			mask.graphics.lineStyle();
			mask.graphics.beginFill(0xFFFFFF, 1);
			mask.graphics.drawRect(70, 167, 330, 454);
			mask.graphics.endFill();
			addElement(mask);
			containerMC.mask = mask;
		}
	}
}