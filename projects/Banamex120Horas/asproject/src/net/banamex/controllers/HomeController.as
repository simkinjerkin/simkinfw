package net.banamex.controllers {

	import com.ui.controllers.BasicMenuController;
	
	import flash.display.InteractiveObject;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class HomeController extends BasicMenuController {

		public static const ON_CLIENT_BUTTON:String = "onClient";
		public static const ON_NO_CLIENT_BUTTON:String = "onNoClient";
		public static const ON_GALLERY_BUTTON:String = "onGallery";
		public static const ON_TERMS_BUTTON:String = "onTerms";

		protected function get clientButton():SimpleButton {
			return containerMC.clientButton;
		}

		protected function get noClientButton():SimpleButton {
			return containerMC.noClientButton;
		}

		protected function get galleryButton():SimpleButton {
			return containerMC.galleryButton;
		}

		protected function get termsButton():SimpleButton {
			return containerMC.termsButton;
		}

		public function HomeController($container:Sprite) {
			super($container);
		}

		override protected function initialize():void {
			addButtonListeners();
		}

		protected function addButtonListeners($add:Boolean = true):void {
			enableButton(clientButton, $add);
			enableButton(noClientButton, $add);
			enableButton(galleryButton, $add);
			enableButton(termsButton, $add);
		}

		override protected function getButtonID($button:InteractiveObject):String {
			switch($button) {
				case clientButton:		return ON_CLIENT_BUTTON;
				case noClientButton:	return ON_NO_CLIENT_BUTTON;
				case galleryButton:		return ON_GALLERY_BUTTON;
				case termsButton:		return ON_TERMS_BUTTON;
			}
			return super.getButtonID($button);
		}

	}
}