package net.banamex.controllers {

	import com.adobe.images.JPGEncoder;
	import com.core.config.URLManager;
	import com.core.system.System;
	import com.facebook.graph.Facebook;
	import com.google.tracking.AnalyticManager;
	import com.net.BasicLoaderEvent;
	import com.net.GraphicLoadManager;
	import com.net.GraphicLoadManagerEventType;
	import com.ui.AlertEvent;
	import com.ui.controllers.BasicController;
	import com.ui.controllers.SiteAlertManager;
	import com.ui.theme.SimpleButtonFactory;
	import com.utils.GraphicUtils;
	
	import dareville.api.facebook.data.albums.FacebookAlbumCreateData;
	import dareville.api.facebook.data.photos.FacebookPhotoCreateData;
	import dareville.api.facebook.services.albums.FacebookAlbumService;
	import dareville.api.facebook.services.photos.FacebookPhotoService;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.security.CertificateStatus;
	import flash.system.LoaderContext;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.utils.ByteArray;
	
	import flashx.textLayout.elements.BreakElement;
	
	import net.banamex.events.EtniaDataEvent;
	import net.banamex.structures.BanamexPromoThumbnailData;
	import net.banamex.structures.PhotoObject;
	import net.core.FacebookGraph;

	public class Banamex120HoursShareController extends BasicController {

		public static const ON_CHANGE_BUTTON:String = "on change button";

		protected var _shareButton:SimpleButton;
		protected var _inviteFriend:SimpleButton;
		protected var _screenGraphic:MovieClip;
		protected var _facebook:FacebookRequestController;
		protected var _serviceController:Banamex120HoursServicesController;
		protected var _savedID:String;
		protected var _graphicLoader:GraphicLoadManager;
		protected var _promotion:BanamexPromoThumbnailData;
		protected var _fbTransactor:FBInfoController;

		public function set promotion($value:BanamexPromoThumbnailData):void {
			_promotion = $value;
			loadImage(_promotion.thumbnailURL);
		}

		protected function get photoContainers():MovieClip {
			return containerMC.screen.photo;
		}
		
		protected function get changePhotoButton():SimpleButton {
			return containerMC.changePhotoButton;
		}
		
		protected function get alert():MovieClip {
			return containerMC.alert;
		}

		protected function get titleTxf():TextField {
			return alert.titleTxf;
		}

		protected function get logoContainer():MovieClip {
			return alert.logoContainer;
		}

		protected function get complementText():MovieClip {
			return alert.complementText;
		}

		public function Banamex120HoursShareController($container:Sprite, $screen:MovieClip) {
			_screenGraphic = $screen;
			super($container);
			alert.visible = false;
		}
		
		public function addImage($userImage:DisplayObject):void{
			createButtons();
			addElement($userImage, true, _screenGraphic.photoContainer);
		}
		
		protected function createButtons():void {
			_shareButton = SimpleButtonFactory.getButton("Publícalo", 90, 565);
			_inviteFriend = SimpleButtonFactory.getButton("Invita a un amigo", 250, 565);
			addElement(_shareButton);
			addElement(_inviteFriend);
			addListeners();
		}
		
		protected function addListeners($add:Boolean = true):void {
			addListener(_shareButton, MouseEvent.CLICK, onClick, $add);
			addListener(_inviteFriend, MouseEvent.CLICK, onClick, $add);
			addListener(changePhotoButton, MouseEvent.CLICK, onClick, $add);
		}
		
		protected function onClick($event:MouseEvent):void {
			switch($event.currentTarget){
				case _shareButton:			showAlertPromo();
											AnalyticManager.getInstance().setTrackEvent('compartirFoto', 'click', 'publicar');
											saveCelebrationAlter();							break;
				case _inviteFriend:			AnalyticManager.getInstance().setTrackEvent('compartirFoto', 'click', 'invitarAmigos');
											inviteFriend();									break;
				case changePhotoButton:		AnalyticManager.getInstance().setTrackEvent('compartirFoto', 'click', 'regresar');
											dispatchEvent(new Event(ON_CHANGE_BUTTON));		break; 
			}
		}
		
		protected function share():void {
			if(!_facebook){
				_facebook = new FacebookRequestController();
			}
			addListener(_facebook, FacebookRequestController.ON_SHARE_SEND, onShareSend);
			_facebook.post(URLManager.getInstance().getPath("serviceBase") + _shareURL);
		}

		protected function onShareSend($event:Event):void {
			addListener(_facebook, FacebookRequestController.ON_SHARE_SEND, onShareSend, false);
		}

		protected function showAlertPromo($show:Boolean = true):void {
			var screen:DisplayObject = GraphicUtils.getClone(_screenGraphic);
			screen.scaleX = .7;
			screen.scaleY = .7;

			titleTxf.autoSize = TextFieldAutoSize.CENTER;
			titleTxf.wordWrap = true;
			titleTxf.multiline = true;
			titleTxf.text = _promotion.promoText;
			complementText.y = titleTxf.y + titleTxf.textHeight + 3;

			($show) ? addElement(screen, true, alert.container.photo) : "";
			alert.visible = $show;
			addListener(alert.closeButton, MouseEvent.CLICK, onAlertClose, $show);
		}

		protected function onAlertClose($event:MouseEvent):void {
			showAlertPromo(false);
		}

		protected function saveCelebrationAlter():void {
			SiteAlertManager.getInstance().showLoader(true, "Guardando foto...", System.stageRoot.stageWidth / 2, (System.stageRoot.stageHeight / 2) + 50);
			var transactor:TransactionManager = new TransactionManager();
			addListener(transactor, TransactionManager.ON_SAVE_CELEBRATION_SUCCESS, onSaveSuccess);
			alert.closeButton.visible = false;
			transactor.saveCelebration(alert);
			alert.closeButton.visible = true;
		}

		protected var _shareURL:String;

		protected function onSaveSuccess($event:BasicLoaderEvent):void {
			alert.closeButton.visible = true;
			SiteAlertManager.getInstance().showLoader(false);
			var transactor:TransactionManager = $event.currentTarget as TransactionManager;
			addListener(transactor, TransactionManager.ON_SAVE_CELEBRATION_SUCCESS, onSaveSuccess, false);
			_savedID = $event.serverResponse.shot.hash_id;
			_shareURL = $event.serverResponse.shot.image;
			uploadAlbumPhoto();
		}
		
		protected function inviteFriend():void {
			var obj:Object = {message: "Aprovecha las 120 horas Banamex",/*max 255*/ title: "120 horas Banamex"/*max 50*/};
			Facebook.ui("apprequests", obj, onInviteFriendsEnd, "iframe");
		}
		
		protected function onInviteFriendsEnd($result:Object):void {}

		protected var _imageLoader: Loader;

		protected function loadImage($imageURL:String):void {
			if(!$imageURL) {
				return;
				throw new Error("Thumbnail: No se puede cargar un $imageURL nulo");
			}
			var urlReq:URLRequest = new URLRequest($imageURL);
			var context: LoaderContext = new LoaderContext(true);
			_imageLoader = new Loader();
			createLoadImageListeners();
			_imageLoader.load(urlReq, context);
		}
		
		protected function createLoadImageListeners($create:Boolean = true):void {
			if(_imageLoader) {
				addListener(_imageLoader.contentLoaderInfo, Event.INIT, onLoadImage, $create);
			}
		}
		
		protected function onLoadImage($event:Event):void {
			//			trace("onLoadImage : " + $event);
			createLoadImageListeners(false);
			if(_imageLoader.content) {
				if(_imageLoader.content is Bitmap) {
					(_imageLoader.content as Bitmap).smoothing = true;
				}
				addElement(_imageLoader.content, true, logoContainer);
			}
			addListener(logoContainer, MouseEvent.CLICK, onLogoClick);
		}

		protected function onLogoClick($event:MouseEvent) :void {
			navigateToURL(new URLRequest(_promotion.promoURL), '_blank');
		}

		protected function uploadAlbumPhoto():void {
			_fbTransactor = new FBInfoController(new Sprite());
			SiteAlertManager.getInstance().showLoader(true, "Obteniendo álbumes de Facebook", System.stageRoot.stageWidth / 2, (System.stageRoot.stageHeight / 2) + 50);
			addListener(_fbTransactor, FBInfoController.ON_GET_ALBUMS_RESPONSE, onGetAlbums);
			_fbTransactor.getAlbums();
		}
		
		protected function onGetAlbums($event:Event):void {
			addListener(_fbTransactor, FBInfoController.ON_GET_ALBUMS_RESPONSE, onGetAlbums, false);
			var appAlbumName:String = "Banamex 120";
			var albumID:String = getAPPAlbumID(_fbTransactor.albums, appAlbumName);
			if(albumID != "") {
				doPhotoUpload(albumID);
			} else {
				createAPPAlbum(appAlbumName);
			}
		}
		
		protected function createAPPAlbum($name:String):void {
			SiteAlertManager.getInstance().showLoader(true, "Creando álbum predeterminado");
			var album:FacebookAlbumCreateData = new FacebookAlbumCreateData($name, getPublishText());
			var service:FacebookAlbumService = new FacebookAlbumService();
			addListener(service, FacebookAlbumService.ON_ALBUM_CREATED, onAlbumCreated);
			service.createAlbum(FacebookGraph.getInstance().accessToken, album);
		}
		
		protected function getPublishText():String {
			return _promotion.promoText;
		}
		
		protected function onAlbumCreated($event:EtniaDataEvent):void {
			var service:FacebookAlbumService = $event.currentTarget as FacebookAlbumService;
			addListener(service, FacebookAlbumService.ON_ALBUM_CREATED, onAlbumCreated, false);
			doPhotoUpload($event.data.id);
		}
		
		protected function getAPPAlbumID($data:Array, $name:String):String {
			for(var i:uint = 0; i < $data.length; i++) {
				if(($data[i] as PhotoObject).name == $name) {
					return ($data[i] as PhotoObject).ID;
				}
			}
			return "";
		}
		
		//"180377235315383"
		protected function doPhotoUpload($albumID:String = "me"):void {
			SiteAlertManager.getInstance().showLoader(true, "Publicando tu foto en Facebook", System.stageRoot.stageWidth / 2, (System.stageRoot.stageHeight / 2) + 50);
			var jpg:JPGEncoder = new JPGEncoder(85);
			alert.closeButton.visible = false;
			var source:ByteArray = jpg.encode(GraphicUtils.getBitmapDataRectAt(alert, 3, 12, 406, 272));
			alert.closeButton.visible = false;
			var photo:FacebookPhotoCreateData = new FacebookPhotoCreateData(getPublishText(), source);
			var service : FacebookPhotoService = new FacebookPhotoService();
			addListener(service, FacebookPhotoService.ON_PHOTO_CREATED, onPhotoCreated);
			service.createPhoto(FacebookGraph.getInstance().accessToken, photo, $albumID);
		}

		protected function onPhotoCreated($event:Event):void {
			var service:FacebookPhotoService = $event.currentTarget as FacebookPhotoService;
			addListener(service, FacebookPhotoService.ON_PHOTO_CREATED, onPhotoCreated, false);
			share();
		}

	}
}