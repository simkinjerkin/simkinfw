package net.banamex.controllers
{
	import com.adobe.serialization.json.JSONEncoder;
	import com.core.config.URLManager;
	import com.net.DataSenderFormat;
	import com.ui.controllers.TransactionController;
	
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	import flashx.textLayout.formats.FormatValue;
	
	public class Banamex120HoursServicesController extends TransactionController {
		public static const ON_COMPLETE:String = "on complete";
		
		public function Banamex120HoursServicesController($container:Sprite) {
			super($container);
			_dataFormat = DataSenderFormat.JSON;
		}

		public function getRemainingTime ():void {
			sendEventRequest(URLManager.getInstance().getPath("getRemainingTime"), new Dictionary(), ON_COMPLETE); 
		}
		
		public function getPromos($cateogories:String):void {
			var data:Dictionary = new Dictionary();
			data.categories = $cateogories;
			sendEventRequest(URLManager.getInstance().getPath("getPromos"), data, ON_COMPLETE); 
		}
		
		public function sendPhoto ():void {
			sendEventRequest(URLManager.getInstance().getPath("sendPhoto"), new Dictionary(), ON_COMPLETE); 
		}

		public function getFaceElements():void {
			sendEventRequest(URLManager.getInstance().getPath("getFaceElements"), new Dictionary(), ON_COMPLETE);
		}
		
		public function getAlertPromo():void {
			sendEventRequest(URLManager.getInstance().getPath("getAlertPromo"), new Dictionary(), ON_COMPLETE);
		}
	}
}