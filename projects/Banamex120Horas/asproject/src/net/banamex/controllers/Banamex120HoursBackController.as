package net.banamex.controllers {
	import com.net.BasicLoaderEvent;
	import com.ui.controllers.BasicController;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	
	import org.osmf.events.LoaderEvent;
	
	public class Banamex120HoursBackController extends BasicController {
		protected var _servicesCtrl:Banamex120HoursServicesController;
		protected var _time:Number;
		
		protected function get counterMc():MovieClip{
			return containerMC.counterMc;
		}
		
		protected function get n1():TextField{
			return counterMc.n1Txf;
		}
		
		protected function get n2():TextField{
			return counterMc.n2Txf;
		}
		
		protected function get n3():TextField{
			return counterMc.n3Txf;
		}
		
		protected function get unit():TextField {
			return counterMc.unitTxf;
		}
		
		public function Banamex120HoursBackController($container:Sprite) {
			super($container);
			getTime();
		}
		
		public function showScreen($show:Boolean = true):void {
			containerMC.screen.visible = $show;
		}
		
		protected function getTime():void {
			if(!_servicesCtrl) {
				_servicesCtrl = new Banamex120HoursServicesController(null);
			}
			addListener(_servicesCtrl, Banamex120HoursServicesController.ON_COMPLETE, onTime);
			_servicesCtrl.getRemainingTime();
		}
		
		protected function onTime($event:BasicLoaderEvent):void {
			addListener(_servicesCtrl, Banamex120HoursServicesController.ON_COMPLETE, onTime, false);
			unit.text = "HORAS";
			_time = $event.serverResponse.remaining;
			calculateHours();
		}
		
		protected function calculateHours():void {
			if(_time > 0){
				parseTime(Math.ceil(_time / 3600));
			}
		}
		
		protected function parseTime($time:Number):void {
			var module:Number = 0;
			
			n1.text = ($time/100).toString();
			module = $time % 100;
			n2.text = (module/10).toString();
			module = module %10;
			n3.text = module.toString();
			
			
			
		}
	}
}