package net.banamex.controllers {

	import com.net.BasicLoaderEvent;
	import com.ui.AlertEvent;
	import com.ui.controllers.BasicController;
	import com.ui.controllers.SiteAlertManager;
	import com.utils.DataUtils;
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import net.banamex.structures.LoggedUserData;
	import net.banamex.theme.BigButtonFactory;
	import net.core.FacebookGraph;

	public class RegisterController extends BasicController {

		public static const ON_REGISTER_SUCCESS:String = "onRegister";
		public static const UNREGISTERED_VOUCHER:String = "unRegistered";

		protected var _userData:LoggedUserData;
		protected var _participateButton:SimpleButton;
		protected var _jumpButton:SimpleButton;

		protected function get nameTxf():TextField {
			return containerMC.nameTxf;
		}

		protected function get emailTxf():TextField {
			return containerMC.emailTxf;
		}

		protected function get yearTxf():TextField {
			return containerMC.yearTxf;
		}

		protected function get monthTxf():TextField {
			return containerMC.monthTxf;
		}

		protected function get dayTxf():TextField {
			return containerMC.dayTxf;
		}

		protected function get amountTxf():TextField {
			return containerMC.amountTxf;
		}

		protected function get mButton():SimpleButton {
			return containerMC.mButton;
		}

		protected function get fButton():SimpleButton {
			return containerMC.fButton;
		}

		protected function get nextButton():SimpleButton {
			return containerMC.nextButton;
		}

		protected function get check():MovieClip {
			//414 y 484
			return containerMC.check;
		}

		public function RegisterController($container:Sprite) {
			super($container);
		}

		public function setDefaultData($data:LoggedUserData = null):void {
			if(!$data) {
				return;
			}
			_userData = $data;
			nameTxf.text = $data.name
			emailTxf.text = $data.mail;
			yearTxf.text = "AAAA";
			monthTxf.text = "MM";
			dayTxf.text = "DD";
			amountTxf.text = "";
			check.x = $data.gender == "F" ? 484 : 414;
			if(_userData.registered) {
				nameTxf.type = TextFieldType.DYNAMIC;
				emailTxf.type = TextFieldType.DYNAMIC;
				addListener(nameTxf, FocusEvent.FOCUS_IN, onTxfFocusIn, false);
				addListener(emailTxf, FocusEvent.FOCUS_IN, onTxfFocusIn, false);
			}
			nameTxf.selectable = !_userData.registered;
			emailTxf.selectable = !_userData.registered;
			addElement(fButton, !_userData.registered);
			addElement(mButton, !_userData.registered);
		}

		override protected function initialize():void {
			super.initialize();
			initializeForm();
			addButtonsListeners();
			showFormButtons();
			addTxfListeners();
		}

		protected function showFormButtons($add:Boolean = true):void {
			if(!_participateButton) {
				_participateButton = BigButtonFactory.getButton("PARTICIPAR", 315, 510);
				_jumpButton = BigButtonFactory.getButton("NO REGISTRAR", 460, 510);
			}
			addElement(_participateButton, $add);
			addElement(_jumpButton, $add);
			addListener(_participateButton, MouseEvent.CLICK, onButtonClick, $add);
			addListener(_jumpButton, MouseEvent.CLICK, onButtonClick, $add);
		}

		protected function initializeForm():void{
			nameTxf.restrict = "a-zA-Z- áéíóú";
			emailTxf.restrict = "a-z0-9@._\\-";
			yearTxf.restrict = "0-9\\-";
			monthTxf.restrict = "0-9\\-";
			dayTxf.restrict = "0-9\\-";
			amountTxf.restrict = "0-9.";
			nameTxf.maxChars = 80;
			emailTxf.maxChars = 80;
			yearTxf.maxChars = 4;
			monthTxf.maxChars = 2;
			dayTxf.maxChars = 2;
			amountTxf.maxChars = 10;
		}

		protected function addButtonsListeners($add:Boolean = true):void {
			addListener(fButton, MouseEvent.CLICK, onButtonClick, $add);
			addListener(mButton, MouseEvent.CLICK, onButtonClick, $add);
		}

		protected function addTxfListeners($add:Boolean = true):void {
			addListener(nameTxf, FocusEvent.FOCUS_IN, onTxfFocusIn, $add);
			addListener(emailTxf, FocusEvent.FOCUS_IN, onTxfFocusIn, $add);
			addListener(yearTxf, FocusEvent.FOCUS_IN, onTxfFocusIn, $add);
			addListener(monthTxf, FocusEvent.FOCUS_IN, onTxfFocusIn, $add);
			addListener(dayTxf, FocusEvent.FOCUS_IN, onTxfFocusIn, $add);
		}

		protected function onTxfFocusIn($event:FocusEvent):void {
			var txf:TextField = $event.target as TextField;
			addListener(txf, FocusEvent.FOCUS_IN, onTxfFocusIn, false);
			addListener(txf, FocusEvent.FOCUS_OUT, onTxfFocusOut);
			if(txf.text == getDefaultText(txf)) {
				txf.text = "";
			}
		}

		protected function onTxfFocusOut($event:FocusEvent):void {
			var txf:TextField = $event.target as TextField;
			addListener(txf, FocusEvent.FOCUS_IN, onTxfFocusIn);
			addListener(txf, FocusEvent.FOCUS_OUT, onTxfFocusOut, false);
			if(txf.text == "") {
				txf.text = getDefaultText(txf);
			}
			if(txf == monthTxf || txf == dayTxf) {
				if(uint(txf.text) < 10 && uint(txf.text) != 0) {
					if(txf.text.length != 2) {
						txf.text = "0" + txf.text;
					}
				}
			}
		}

		protected function getDefaultText($txf:TextField):String {
			switch($txf) {
				case nameTxf:			return _userData.name;
				case emailTxf:			return _userData.mail;
				case yearTxf:			return "AAAA";
				case monthTxf:			return "MM";
				case dayTxf:			return "DD";
			}
			return "";
		}

		protected function onButtonClick($event:MouseEvent):void {
			switch($event.currentTarget) {
				case _participateButton:			doRegister();											break;
				case _jumpButton:					dispatchEvent(new Event(UNREGISTERED_VOUCHER));			break;
				case fButton:						check.x = 484;											break;
				case mButton:						check.x = 414;											break;
			}
		}

		protected function isDataValid():Boolean {
			if(!DataUtils.isValidEmail(emailTxf.text)) {
				SiteAlertManager.getInstance().showAlert("Ingrese un correo electrónico válido.", ["OK"], onAlertClose);
				return false;
			}
			trace(Number(amountTxf.text));
			if(!DataUtils.isValidDateAAAA_MM_DD(getDateStr())) {
				SiteAlertManager.getInstance().showAlert("Ingrese una fecha válida", ["OK"], onAlertClose);
				return false;
			}
			if(amountTxf.text == "" || Number(amountTxf.text) == 0 || isNaN(Number(amountTxf.text))) {
				SiteAlertManager.getInstance().showAlert("Ingrese un monto válido", ["OK"], onAlertClose);
				return false;
			}
			return true;
		}

		protected function onAlertClose($event:AlertEvent):void {}

		protected function doRegister():void {
			if(!isDataValid()) {
				return;
			}
			var transactor:TransactionManager = new TransactionManager();
			addListener(transactor, TransactionManager.ON_VOUCHER_REGISTER_SUCCESS, onVoucherRegisterSuccess);
			addListener(transactor, TransactionManager.ON_REGISTER_SUCCESS, onRegisterSuccess);
			if(_userData.registered) {
				transactor.registerVoucher(amountTxf.text, getDateStr());
			} else {
				transactor.sendRegister(nameTxf.text, emailTxf.text, check.x < 450 ? "M" : "F");
			}
		}

		protected function onRegisterSuccess($event:BasicLoaderEvent):void {
			var transactor:TransactionManager = $event.currentTarget as TransactionManager;
			addListener(transactor, TransactionManager.ON_REGISTER_SUCCESS, onRegisterSuccess, false);
			transactor.registerVoucher(amountTxf.text, getDateStr());
		}

		protected function getDateStr():String {
			return yearTxf.text + "-" + monthTxf.text + "-" + dayTxf.text;
		}

		protected function onVoucherRegisterSuccess($event:BasicLoaderEvent):void {
			var transactor:TransactionManager = $event.currentTarget as TransactionManager;
			addListener(transactor, TransactionManager.ON_VOUCHER_REGISTER_SUCCESS, onVoucherRegisterSuccess, false);
			addListener(transactor, TransactionManager.ON_REGISTER_SUCCESS, onRegisterSuccess, false);
			dispatchEvent(new Event(ON_REGISTER_SUCCESS));
		}

	}

}