package net.banamex.controllers {

	import com.ui.controllers.BasicController;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;

	public class BackgroundController extends BasicController {

//		protected function get dialog():MovieClip {
//			return containerMC.dialog;
//		}

		protected function get logo():MovieClip {
			return containerMC.logo;
		}

		protected function get dialogBack():MovieClip {
			return containerMC.dialogBack;
		}

		public function BackgroundController($container:Sprite) {
			super($container);
//			trace(dialog.x + " :: " + dialogBack.x);
//			dialog.mouseEnabled = false;
//			dialog.mouseChildren = false;
//			addElement(dialog, false);
			addElement(dialogBack, false);
			addElement(logo, false);
		}

		public function showReduce($value:Boolean = true):void {
//			dialogBack.scaleX = dialog.scaleX = $value ? .88 : 1;
//			dialog.x = $value ? 105 : 61;
//			dialogBack.x = $value ? 94 : 50;
		}

		public function setInitView():void {
			showDialog(false);
			addElement(logo, false);
			containerMC.gotoAndStop(1);
//			dialog.mouseEnabled = false;
//			dialog.mouseChildren = false;
//			addElement(dialog, false);
			addElement(dialogBack, false);
			addElement(logo, false, panelContainer);
		}

		public function setRegisterView():void {
			containerMC.play();
			showDialog(true, 176);
			addElement(logo, true, panelContainer);
		}

		protected function showDialog($add:Boolean = true, $y:Number = 0):void {
//			dialogBack.y = dialog.y = $y;
//			addElement(dialog, $add, panelContainer);
			addElement(dialogBack, $add);
		}

		protected function get panelContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("panelContainer");
		}

	}
}