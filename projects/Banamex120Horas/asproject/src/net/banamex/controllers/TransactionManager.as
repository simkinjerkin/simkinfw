package net.banamex.controllers {
0
	import com.adobe.images.PNGEncoder;
	import com.core.config.URLManager;
	import com.core.system.System;
	import com.dynamicflash.util.Base64;
	import com.net.BasicLoader;
	import com.net.BasicLoaderEvent;
	import com.net.DataSender;
	import com.net.DataSenderFormat;
	import com.net.ServerResponse;
	import com.ui.controllers.SiteAlertManager;
	import com.ui.controllers.TransactionController;
	import com.utils.GraphicUtils;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import net.core.FacebookGraph;

	public class TransactionManager extends TransactionController {

		public static const ON_LOGIN_SUCCESS:String = "loginsccss";
		public static const ON_REGISTER_SUCCESS:String = "registersccss";
		public static const ON_VOUCHER_REGISTER_SUCCESS:String = "vregistersccss";
		public static const ON_GALLERY_DATA_SUCCESS:String = "gdatasccss";
		public static const ON_SAVE_CELEBRATION_SUCCESS:String = "saveCsccss";
		public static const ON_SAVE_THUMB_CELEBRATION_SUCCESS:String = "saveThumbCsccss";
		public static const ON_SAVE_THUMB_CELEBRATION_ERROR:String = "saveThumbCError";
		public static const ON_VOTE_SUCCESS:String = "votesccss";
		public static const ON_MOVES_DATA_SUCCESS:String = "movessccss";
		public static const ON_GET_CELEBRATION_DATA_SUCCESS:String = "gceldatassccss";

		protected var _loader:URLLoader;

		public function getMovementConfig():Boolean {
			return sendEventRequest(URLManager.getInstance().getPath("configDir") + "movConfig.json", getEmptyData(), ON_MOVES_DATA_SUCCESS);
		}

		public function sendLogin():Boolean {
			var data:Dictionary = getBasicData();
			data.name = FacebookGraph.getInstance().userData.name;
			data.email = FacebookGraph.getInstance().userData.mail ? FacebookGraph.getInstance().userData.mail : "";
//			dispatchEvent(new BasicLoaderEvent(ON_LOGIN_SUCCESS, "", 0, 0, 0, null, DataSender.getJSONServerResponse('{"status":"success", "message":"El usuario ya existía en bd "}'), null));
//			return false;
			return sendEventRequest(URLManager.getInstance().getPath("loginService"), data, ON_LOGIN_SUCCESS);
		}

		public function getCelebrationInfo($ID:String):Boolean {
			var data:Dictionary = getBasicData();
			data.id = $ID;
			return sendEventRequest(URLManager.getInstance().getPath("celebrationInfoService"), data, ON_GET_CELEBRATION_DATA_SUCCESS);
		}

		public function sendRegister($name:String, $mail:String, $gender:String):Boolean {
			var data:Dictionary = new Dictionary();/*getBasicData();*/
			data.gender = $gender;
			data.name = $name;
			data.email = $mail;
			return sendEventRequest(URLManager.getInstance().getPath("registerService"), data, ON_REGISTER_SUCCESS);
		}

		public function saveCelebration($do:DisplayObject):Boolean {
			var data:Dictionary = new Dictionary();
			data["FacebookUser[uid]"] = FacebookGraph.getInstance().userData.ID;
			data["FacebookUser[first_name]"] = FacebookGraph.getInstance().userData.firstName;
			data["FacebookUser[last_name]"] = FacebookGraph.getInstance().userData.lastName;
			data["FacebookUser[username]"] = FacebookGraph.getInstance().userData.userName;
			data["FacebookUser[email]"] = FacebookGraph.getInstance().userData.mail;
			data["FacebookUser[birth_date]"] = "1999-12-12";
			data["FacebookUser[gender]"] = FacebookGraph.getInstance().userData.gender;
			data["Shot[image]"] = Base64.encodeByteArray(PNGEncoder.encode(GraphicUtils.getBitmapDataRectAt($do, 3, 12, 406, 272)));
			return sendEventRequest(URLManager.getInstance().getPath("saveService"), data, ON_SAVE_CELEBRATION_SUCCESS);
		}

		public function saveAlternative($do:DisplayObject):void {
			var header:URLRequestHeader = new URLRequestHeader("Content-type", "application/octet-stream");
			var request:URLRequest = new URLRequest();
			request.requestHeaders.push(header);
			request.url = URLManager.getInstance().getPath("saveService") + "?fbid=" + FacebookGraph.getInstance().userData.ID + "&auth=" + FacebookGraph.getInstance().accessToken + "&seq=1";
			request.method = URLRequestMethod.POST;
			var data:Dictionary
			request.data = PNGEncoder.encode(GraphicUtils.getBitmapData($do));
			_onCompleteTypeEvent = ON_SAVE_CELEBRATION_SUCCESS;
			_onError = onRequestError;
			_loader = new URLLoader();
			_loader.dataFormat = URLLoaderDataFormat.BINARY;
			loaderListeners();
			_loader.load(request);
			SiteAlertManager.getInstance().showLoader(true, getCurrentLoadingMessage(_onCompleteTypeEvent), System.stageRoot.stageWidth / 2, System.stageRoot.stageHeight / 2, true);
		}

		public function saveThumbnail($do:DisplayObject, $ID:String):void {
//			addElement($do, true, alertsContainer);
			var header:URLRequestHeader = new URLRequestHeader("Content-type", "application/octet-stream");
			var request:URLRequest = new URLRequest();
			request.requestHeaders.push(header);
			request.url = URLManager.getInstance().getPath("saveThumbService") + "?fbid=" + FacebookGraph.getInstance().userData.ID + "&auth=" + FacebookGraph.getInstance().accessToken + "&id=" + $ID;
			request.method = URLRequestMethod.POST;
			request.data = PNGEncoder.encode(GraphicUtils.getBitmapData($do));
			_onCompleteTypeEvent = ON_SAVE_THUMB_CELEBRATION_SUCCESS;
			_onErrorTypeEvent = ON_SAVE_THUMB_CELEBRATION_ERROR;
			_loader = new URLLoader();
			_loader.dataFormat = URLLoaderDataFormat.BINARY;
			loaderListeners();
			_loader.load(request);
			SiteAlertManager.getInstance().showLoader(true, getCurrentLoadingMessage(_onCompleteTypeEvent), System.stageRoot.stageWidth / 2, System.stageRoot.stageHeight / 2, true);
		}

		protected function loaderListeners($add:Boolean = true):void {
			addListener(_loader, Event.COMPLETE, onImageCompleteHandler, $add);
			addListener(_loader, SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler, $add);
			addListener(_loader, IOErrorEvent.IO_ERROR, ioErrorHandler, $add);
		}

		protected function securityErrorHandler($event:SecurityErrorEvent):void {
			onImageLoaderFail($event);
		}

		protected function ioErrorHandler($event:IOErrorEvent):void {
			onImageLoaderFail($event);
		}

		protected function onImageLoaderFail($event:Event):void {
			loaderListeners(false);
			var event:BasicLoaderEvent = new BasicLoaderEvent(_onErrorTypeEvent != "" ? _onErrorTypeEvent : XML_DATA_ERROR,
				"URL", 0, 0, 0, null, null, null);
			doOnErrorResponse(event);
		}

		protected function onImageCompleteHandler($event:Event):void {
			loaderListeners(false);
			var serverResponse:ServerResponse = DataSender.getJSONServerResponse(($event.target.data as ByteArray).toString());
			onSendRequestComplete(new BasicLoaderEvent(ON_SAVE_CELEBRATION_SUCCESS, URLManager.getInstance().getPath("saveService"),
									0, $event.target.bytesLoaded, $event.target.bytesTotal, ($event.target.data as ByteArray).toString(),
									serverResponse, null));
		}

		public function sendVote($ID:String):Boolean {
			var data:Dictionary = getBasicData();
			data.id = $ID;
			return sendEventRequest(URLManager.getInstance().getPath("voteService"), data, ON_VOTE_SUCCESS);
		}

		public function getGalleryData($page:uint = 1, $filter:String = "date"):Boolean {
			trace($page);
			var data:Dictionary = getBasicData();
			data.pag = $page;
			data.filter = $filter;
			return sendEventRequest(URLManager.getInstance().getPath("galleryDataService"), data, ON_GALLERY_DATA_SUCCESS);
		}

		public function registerVoucher($amount:String, $date:String):Boolean {
			var data:Dictionary = getBasicData();
			data.amount = $amount;
			data.date = $date;
			return sendEventRequest(URLManager.getInstance().getPath("voucherService"), data, ON_VOUCHER_REGISTER_SUCCESS);
		}

		public function TransactionManager() {
			super(new Sprite());
			_dataFormat = DataSenderFormat.JSON;
		}

		protected function getCurrentLoadingMessage($type:String):String {
			switch($type) {
				case ON_LOGIN_SUCCESS:							return "Cargando información de usuario";
				case ON_REGISTER_SUCCESS:						return "Guardando información de usuario";
				case ON_VOUCHER_REGISTER_SUCCESS:				return "Registrando información del voucher";
				case ON_GALLERY_DATA_SUCCESS:					return "Obteniendo información de galeria de usuarios";
				case ON_SAVE_CELEBRATION_SUCCESS:				return "Guardando tu celebración";
				case ON_SAVE_THUMB_CELEBRATION_SUCCESS:			return "Guardando tu celebración";
				case ON_VOTE_SUCCESS:							return "Guardando tu voto";
				case ON_MOVES_DATA_SUCCESS:						return "Cargando información de sistema";
			}
			return "";
		}

		override protected function onSendRequestComplete($event:BasicLoaderEvent):void {
			SiteAlertManager.getInstance().showLoader(false);
			if(!$event.serverResponse["success"]) {
				var event:BasicLoaderEvent = new BasicLoaderEvent(_onErrorTypeEvent,
					$event.url, $event.retry, $event.bytesLoaded,
					$event.bytesTotal, $event.item, $event.serverResponse,
					$event.data);
				_currentErrorID = $event.serverResponse[_errorConstant];
				doOnErrorResponse(event);
				return;
			}
			super.onSendRequestComplete($event);
		}

		override protected function getMessage($event:BasicLoaderEvent):String {
			if($event.serverResponse && $event.serverResponse["message"]) {
				return $event.serverResponse["message"];
			}
			return super.getMessage($event);
		}

		protected function getBasicData():Dictionary {
			var data:Dictionary = new Dictionary();
			data.uid = FacebookGraph.getInstance().userData.ID;
			data.first_name = FacebookGraph.getInstance().userData.firstName;
			data.last_name = FacebookGraph.getInstance().userData.lastName;
			data.username = FacebookGraph.getInstance().userData.name;
			data.gender = FacebookGraph.getInstance().userData.gender;
			return data;
		}

		protected function get alertsContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("alertsContainer");
		}

	}

}