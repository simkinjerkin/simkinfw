package net.banamex.controllers {

	import com.facebook.graph.Facebook;
	import com.ui.controllers.BasicController;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import net.banamex.structures.FeedObject;
	import net.banamex.structures.PhotoObject;
	import net.structures.DetailFBUserData;

	public class FBInfoController extends BasicController {

		public static const ON_GET_FRIENDS_RESPONSE:String = "getFriends";
		public static const ON_GET_FEED_RESPONSE:String = "getFeed";
		public static const ON_GET_USER_DETAIL_RESPONSE:String = "getUserDetail";
		public static const ON_GET_USER_PHOTOS_RESPONSE:String = "getUserPhotos";
		public static const ON_GET_ALBUMS_RESPONSE:String = "getAlbums";

		public static const ON_FB_ERROR:String = "onFBError";

		protected var _feed:Array;
		protected var _currentFriends:Array;
		protected var _currentUserID:String;
		protected var _currentUserDetail:DetailFBUserData;
		protected var _photos:Array;
		protected var _albums:Array;

		public function get currentUserDetail():DetailFBUserData {
			return _currentUserDetail;
		}

		public function get albums():Array {
			return _albums;
		}

		public function get photos():Array {
			return _photos;
		}

		public function get feed():Array {
			return _feed;
		}

		public function get friendlist():Array {
			return _currentFriends;
		}

		public function FBInfoController($container:Sprite) {
			super($container);
		}

		public function sendEnmaskara():void {
			Facebook.api("/me/simkin_testing_graph:enmaskara", onSendEnmaskaraResponse);
		}

		protected function onSendEnmaskaraResponse($result:Object, $fail:Object):void {
			if($result) {
				
			} else {
				
			}
		}

		public function getFriends($ID:String = "me", $fields:String = ""):void {
			_currentUserID = $ID;
			Facebook.api("/" + $ID + "/friends", onFriendsResponse, $fields == "" ? null : {"fields":$fields});
		}

		public function getProfileFeed($ID:String = "me"):void {
			_currentUserID = $ID;
			Facebook.api("/" + $ID + "/feed", onProfileFeedResponse);
		}

		public function getUserDetail($ID:String = "me", $fields:String = "id,name"):void {
			_currentUserID = $ID;
			Facebook.api("/" + $ID, onUserDetailResponse, {"fields":$fields});
		}

		public function getUserPhotos($ID:String = "me"):void {
			_currentUserID = $ID;
			Facebook.api("/" + $ID + "/photos", onUserPhotosResponse);//, {"offset":8});
		}

		public function getAlbums($ID:String = "me"):void {
			_currentUserID = $ID;
			Facebook.api("/" + $ID + "/albums", onGetAlbumsResponse);//, {"offset":8});
		}

		protected function onGetAlbumsResponse($result:Object, $fail:Object):void {
			if($result) {
				_albums = new Array();
				for(var i:uint = 0; i < $result.length; i++) {
					_albums.push(new PhotoObject($result[i]));
				}
				dispatchEvent(new Event(ON_GET_ALBUMS_RESPONSE));
			} else {
				dispatchEvent(new Event(ON_FB_ERROR));
			}
		}

		protected function onUserPhotosResponse($result:Object, $fail:Object):void {
			if($result) {
				_photos = new Array();
				for(var i:uint = 0; i < $result.length; i++) {
					_photos.push(new PhotoObject($result[i]));
				}
				dispatchEvent(new Event(ON_GET_USER_PHOTOS_RESPONSE));
			} else {
				dispatchEvent(new Event(ON_FB_ERROR));
			}
		}

		protected function onFriendsResponse($result:Object, $fail:Object):void {
			if($result) {
				_currentFriends = new Array();
				for(var i:uint = 0; i < $result.length; i++) {
					_currentFriends.push(new DetailFBUserData($result[i]));
				}
				dispatchEvent(new Event(ON_GET_FRIENDS_RESPONSE));
			} else {
				dispatchEvent(new Event(ON_FB_ERROR));
			}
			
		}

		protected function onProfileFeedResponse($result:Object, $fail:Object):void {
			if($result) {
				_feed = new Array();
				for(var i:uint = 0; i < $result.length; i++) {
					_feed.push(new FeedObject($result[i]));
				}
				dispatchEvent(new Event(ON_GET_FEED_RESPONSE));
			} else {
				dispatchEvent(new Event(ON_FB_ERROR));
			}
		}

		protected function onUserDetailResponse($result:Object, $fail:Object):void {
			if($result) {
				_currentUserDetail = new DetailFBUserData($result);
				dispatchEvent(new Event(ON_GET_USER_DETAIL_RESPONSE));
			} else {
				dispatchEvent(new Event(ON_FB_ERROR));
			}
		}

	}
}