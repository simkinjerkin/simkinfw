package net.banamex.controllers {

	import com.components.BasicScroll;
	import com.components.BasicScrollEvent;
	import com.core.config.URLManager;
	import com.graphic.Banamex120HorasRotatableDynamicSprite;
	import com.graphics.gallery.SimpleGallery;
	import com.graphics.gallery.Thumbnail;
	import com.graphics.gallery.ThumbnailData;
	import com.graphics.gallery.ThumbnailEvent;
	import com.graphics.gallery.ThumbnailEventType;
	import com.net.BasicLoaderEvent;
	import com.ui.controllers.BasicController;
	import com.utils.GraphicUtils;
	import com.utils.MathUtils;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.ui.Mouse;
	
	import net.banamex.gallery.BanamexElementFaceThumbnail;
	import net.graphics.AdjustHeadThumbnail;

	public class AdjustHeadController extends BasicController {

		protected static const TIMES_GREATER:uint = 2;
		protected static const MAX_HEAD_SCALE:uint = 2;

		protected var _viewer:AdjustHeadThumbnail;
		protected var _parametersScrap:Rectangle = new Rectangle(354, 264, 330, 270);
		protected var _parameters:Rectangle = new Rectangle(0, -6, 330, 270);
		protected var _maskParameters:Rectangle = new Rectangle(0, -6, 330, 270);
		protected var _rotateValue:Number = 0;
		protected var _sizeValue:Number = 1;
		protected var _eyes:MovieClip;
		protected var _servicesCtrl:Banamex120HoursServicesController;
		protected var _dataArray:Array;
		protected var _galleryEyes:SimpleGallery;
		protected var _galleryMouths:SimpleGallery;
		protected var _isEyes:Boolean;
		protected var _defaultWidth:Number = 50;
		protected var _defaultHeight:Number = 50;
		protected var _isFirst:Boolean = true;
		
		public function get currentHead():DisplayObject {
			var visible:Boolean = true;
			if(_viewer && !_viewer.parent && !_lastWebcam) {
				showControllers(true);
				visible = false;
			}
			marks.visible = false;
			var head:DisplayObject;
			head = GraphicUtils.getCloneRectAt(containerMC, _parametersScrap.x, _parametersScrap.y, _maskParameters.width, _maskParameters.height);
//			addElement(GraphicUtils.getCloneRectAt(containerMC, _parametersScrap.x, _parametersScrap.y, _maskParameters.width, _maskParameters.height), true, panelContainer);
			marks.visible = true;
			if(!visible) {
				showControllers(false);
			}
			return head;
		}

		public function get viewer():AdjustHeadThumbnail {
			return _viewer;
		}

		protected function get panelContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("panelContainer");
		}

		protected function get eyesAdjust():MovieClip {
			return containerMC.eyesAdjust;
		}

		protected function get adjustHead():MovieClip {
			return containerMC.adjustHead;
		}

		protected function get marks():MovieClip {
			return adjustHead.marks;
		}

		protected function get zoomInButton():SimpleButton {
			return sliders.controlsBar.zoomInButton;
		}
		
		protected function get zoomOutButton():SimpleButton {
			return sliders.controlsBar.zoomOutButton;
		}
		
		protected function get rightRotateButton():SimpleButton {
			return sliders.controlsBar.rightRotateButton;
		}
		
		protected function get leftRotateButton():SimpleButton {
			return sliders.controlsBar.leftRotateButton;
		}

		protected function get cursor():MovieClip {
			return sliders.cursor;
		}

		protected function get sliders():MovieClip {
			return adjustHead.sliders;
		}

		protected var _photoContainer:MovieClip;

		public function AdjustHeadController($container:Sprite, $photoContainer:MovieClip) {
			_photoContainer = $photoContainer;
			super($container);
		}

		public function showControllers($add:Boolean = true):void {
			addElement(_viewer, $add, _photoContainer);
			addElement(marks, $add, adjustHead);
			addElement(sliders, $add, adjustHead);
		}

		public function showAdjustViewer($add:Boolean = true):void {
			addElement(_viewer, $add, _photoContainer);
			addElement(sliders, $add, adjustHead);
		}

		public function activeCursor($active:Boolean = true):void {
			addListener(marks, Event.ENTER_FRAME, whieViewerAdded, $active);
		}

		protected function whieViewerAdded($event:Event):void {
			if(MathUtils.isValueBetween(_viewer.mouseX, 0, _parameters.width) &&
				MathUtils.isValueBetween(_viewer.mouseY, 0, _parameters.height)) {
				cursor.visible = true;
				cursor.x = _viewer.mouseX + 24;
				cursor.y = _viewer.mouseY + 19;
				Mouse.hide();
			} else {
				Mouse.show();
				cursor.visible = false;
			}
		}

		protected var _lastWebcam:Boolean = false;

		public function setHead($do:DisplayObject, $webcam:Boolean = false):void {
			addElement($do, true, panelContainer);
			showViewer();
			_viewer.setUserImage($do, _parameters, $webcam, 0, 0, _maskParameters);
			var minScale:Number;
			var middle:Number;
			if(!$webcam) {
				minScale = Math.max(_parameters.width / $do.width, _parameters.height / $do.height);
				middle = ((MAX_HEAD_SCALE - minScale) / 2) + .1;
			} else {
				minScale = .5;
				middle = 1;
				_viewer.dynamicImage.x = _maskParameters.x;
				_viewer.dynamicImage.y = _maskParameters.y;
			}
			var maxScale:Number = Math.max((_parameters.width * TIMES_GREATER) / $do.width, (_parameters.height * TIMES_GREATER) / $do.height);
			_viewer.resizeUserImage(middle);
			_lastWebcam = false;
		}

		public function setWebcamHolder($webcamContainer:Sprite, $add:Boolean = true):void {
//			addElement($do, true, panelContainer);
			showControllers(false);
			addElement($webcamContainer, $add, adjustHead);
			addElement($webcamContainer.mask, $add, adjustHead);
//			addElement(marks, $add, adjustHead);
			_lastWebcam = true;
		}

		override protected function initialize():void {
			super.initialize();
			marks.buttonMode = true;
			marks.useHandCursor = true;
			marks.mouseChildren = false;
			marks.mouseEnabled = false;
			cursor.mouseEnabled = false;
			sliders.mouseEnabled = false;
			adjustHead.mouseEnabled = false;
			adjustHead.parent.mouseEnabled = false;
			showScrolls();
			addElement(eyesAdjust, false);
		}

		protected function showScrolls($add:Boolean = true):void {
			addScrollsListeners($add);
		}

		protected function addScrollsListeners($add:Boolean = true):void {
			addListener(zoomInButton, MouseEvent.CLICK, onClick, $add);
			addListener(zoomOutButton, MouseEvent.CLICK, onClick, $add);
			addListener(rightRotateButton, MouseEvent.CLICK, onClick, $add);
			addListener(leftRotateButton, MouseEvent.CLICK, onClick, $add);
			cursor.visible = $add;
		}

		protected function onClick($event:MouseEvent):void {
			if(_isFirst) {
				_sizeValue = _viewer.dynamicImage.scaleX;
				_isFirst = false;
			}
			switch($event.currentTarget){
				case zoomInButton:		_sizeValue += .25;
										_viewer.resizeUserImage(_sizeValue);	break;
				case zoomOutButton:		trace("ScaleImage:::" + _viewer.dynamicImage.scaleX);
										if(_viewer.dynamicImage.scaleX > 1){
											(_sizeValue > 0) ? _sizeValue -= .25 :"";
											_viewer.resizeUserImage(_sizeValue);	
										}
										break;
				case rightRotateButton:	_rotateValue += 10;
										_viewer.rotateUserImage(_rotateValue);	break;
				case leftRotateButton:	_rotateValue -= 10;
										_viewer.rotateUserImage(_rotateValue);	break;
			}		
		}

		protected function showViewer($add:Boolean = true):void {
			if(!_viewer) {
				var background:Sprite = new Sprite();
				background.graphics.beginFill(1, 1);
				background.graphics.drawRect(0, 0, 100, 100);
				background.graphics.endFill();
				_viewer = new AdjustHeadThumbnail("viewer", background);
				_viewer.buttonEnabled = false;
				_viewer.enabled = false;
				_viewer.dynamicSpriteClass = Banamex120HorasRotatableDynamicSprite;
			}
			showControllers($add);
		}

	}
}