package net.banamex.controllers {

	import com.core.config.URLManager;
	import com.core.system.System;
	import com.facebook.graph.Facebook;
	import com.google.tracking.AnalyticManager;
	import com.ui.AlertEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.net.LocalConnection;
	
	import net.core.FacebookGraph;
	import net.ui.BasicFacebookGraphController;
	
	public class FacebookRequestController extends BasicFacebookGraphController {

		public static const ON_SHARE_SEND:String = "on send share";
		
		protected var _currentDataID:String;
		
		public function set currentDataID($value:String):void {
			_currentDataID = $value;
		}
		
		public function FacebookRequestController() {
			super(new Sprite());
		}

		public function post($photoID:String):void {
//			AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'CompartirAplicacion');
			var retrieveURL:String = "" + URLManager.getInstance().getPath("retrieveURL");
			streamPublish("", $photoID, retrieveURL,
				"Aprovecha las 120 horas Banamex", "", "Siguete sorprendiendo con las  120 horas exclusivas.",
				"Source");				
		}

		public function shareVideo($ID:String):void {
			AnalyticManager.getInstance().setTrackEvent('botones', 'click', 'CompartirVideo');
			var retrieveURL:String = "http://simkinbravo.com/Banamex/" + "?ID=" + $ID;
			streamPublish("", ((new LocalConnection()).domain != "localhost" ? "" : "http://simkinbravo.com/Banamex/") + URLManager.getInstance().getPath("imagesDir") + "fbThumbnail.png", retrieveURL,
				"Celebrando con el chicharito", "", FacebookGraph.getInstance().userData.name + " celebró como el chicharito.",
				"Source");
		}

		protected function onAlert($event:AlertEvent):void{}

		override protected function onPostResponse($result:Object):void {
			dispatchEvent(new Event(ON_SHARE_SEND));
		}
	}
}