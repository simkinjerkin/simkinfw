package net.banamex.controllers {

	import com.core.config.URLManager;
	import com.core.system.System;
	import com.geom.ComplexPoint;
	import com.google.tracking.AnalyticManager;
	import com.graphics.gallery.SimpleGallery;
	import com.graphics.gallery.ThumbnailData;
	import com.graphics.gallery.ThumbnailEvent;
	import com.graphics.gallery.ThumbnailEventType;
	import com.net.BasicLoader;
	import com.net.BasicLoaderEvent;
	import com.net.GraphicLoadManager;
	import com.net.GraphicLoadManagerEventType;
	import com.ui.AlertEvent;
	import com.ui.controllers.BasicController;
	import com.ui.controllers.SiteAlertManager;
	import com.ui.controllers.mvc.BasicFormStates;
	import com.ui.controllers.mvc.fbpicture.BasicFBPicturePickerStates;
	import com.ui.controllers.mvc.interfaces.IController;
	import com.ui.controllers.mvc.interfaces.IModel;
	import com.ui.controllers.mvc.userpicture.BasicFileReferenceModel;
	import com.ui.controllers.mvc.userpicture.UserPicturePickerStates;
	import com.ui.controllers.mvc.webcam.BasicWebcamModel;
	import com.ui.controllers.mvc.webcam.BasicWebcamStates;
	import com.ui.theme.SimpleButtonFactory;
	import com.utils.GraphicUtils;
	import com.utils.MathUtils;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.Video;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import net.banamex.gallery.BanamexElementFaceThumbnail;
	import net.banamex.mvc.BanamexFBGPhotoPickerModel;
	import net.banamex.mvc.BanamexFBGraphPicturePickerController;
	import net.banamex.mvc.BanamexPicturePickerGraphView;
	import net.ui.mvc.fbpicture.BasicFBGraphPicturePickerController;
	import net.ui.mvc.fbpicture.FacebookGraphPhotoPickerModel;
	import net.ui.mvc.userpicture.BasicFileReferenceControllerGraph;
	import net.ui.mvc.userpicture.UserPicturePickerGraphController;
	import net.ui.mvc.userpicture.UserPicturePickerGraphModel;
	import net.ui.mvc.userpicture.views.StandardPicturePickerGraphView;

	public class PicturePickerController extends BasicController {

		public static const ON_HOME_CLICK:String = "homeClick";
		public static const ON_HEAD_ADJUST:String = "onHeadAdjust";
		public static const HIDE_SHARE:String = "hideShare";
		public static const ON_CELEBRATION_MADE:String = "onCelebrationSuccess";

		protected var _backButton:SimpleButton;
		protected var _nextButton:SimpleButton;
		protected var _userPPView:StandardPicturePickerGraphView;
		protected var _currentStep:uint = 0;
		protected var _adjustController:AdjustHeadController;
		protected var _screenGraphic:MovieClip;
		protected var _ppModel:UserPicturePickerGraphModel;
		protected var _alternativeButton:SimpleButton;
		protected var _shotAgainButton:SimpleButton;
		protected var _savedID:String;
		protected var _lastHead:DisplayObject;
		protected var _captureTimer:Timer;
		protected var _userImage:DisplayObject;

		public function get userImage():DisplayObject{
			return _userImage;
		}
//		public function get sequenceData():SWFSecuenceData {
//			var seq:SWFSecuenceData = new SWFSecuenceData();
//			seq.ID = _savedID;
//			seq.head = _lastHead;
//			seq.movements = _configurator.movements;
//			return  seq;
//		}

		protected function get homeGraphic():MovieClip {
			return containerMC.homeGraphic;
		}

		protected function get fbGraphic():MovieClip {
			return containerMC.fbGraphic;
		}

		protected function get adjustGraphic():MovieClip {
			return containerMC.adjustGraphic;
		}

		protected function get counter():MovieClip {
			return fbGraphic.counter;
		}

/*
		protected function get typeButton():SimpleButton {
			return containerMC.typeButton;
		}

		protected function get adjustButton():SimpleButton {
			return containerMC.adjustButton;
		}

		protected function get marksButton():SimpleButton {
			return containerMC.marksButton;
		}
*/
		protected function get stepMak():MovieClip {
			return containerMC.step;
		}

		public function PicturePickerController($container:Sprite, $screen:MovieClip) {
			_screenGraphic = $screen;
			super($container);
		}

		public function reinit():void {
			showHomeButtons(true);
			createUserPicturePicker();
		}

		public function doBackButtonAction():void {
			doBackButton();
		}

		override protected function initialize():void {
			super.initialize();
			_backButton = SimpleButtonFactory.getButton("REGRESAR");
			_nextButton = SimpleButtonFactory.getButton("SIGUIENTE");
			_alternativeButton = SimpleButtonFactory.getButton("TOMAR FOTO");
			_shotAgainButton = SimpleButtonFactory.getButton("TOMAR NUEVAMENTE");
			_backButton.x = 100;
			_nextButton.x = _backButton.x + 140;
			_shotAgainButton.x = _alternativeButton.x = _nextButton.x;
			_shotAgainButton.y = _alternativeButton.y = _nextButton.y = _backButton.y = 610;
			addElement(counter, false, fbGraphic);
			createUserPicturePicker();
			showFBStage(false);
			addElement(adjustGraphic, false);
			setCurrentStep(0);
		}
		
		protected function setYButtons($y:Number = 610):void {
			_shotAgainButton.y = _alternativeButton.y = _nextButton.y = _backButton.y = $y;
		}

		protected function showCounter($add:Boolean = true):void {
			addElement(counter, $add);
		}

		protected function addBackFowardButtons($back:Boolean = true, $foward:Boolean = true, $pic:Boolean = false):void {
			addElement(_backButton, $back);
			addElement(_nextButton, $foward);
			addElement(_alternativeButton, $pic);
			addListener(_backButton, MouseEvent.CLICK, onButtonClick, $back);
			addListener(_nextButton, MouseEvent.CLICK, onButtonClick, $foward);
			addListener(_alternativeButton, MouseEvent.CLICK, onButtonClick, $pic);
		}

		protected function onButtonClick($event:MouseEvent):void {
			switch($event.currentTarget) {
				case _backButton:			doBackButton();								break;
				case _shotAgainButton:		addBackFowardButtons(true, false);
				case _alternativeButton:	setCaptureTimer();							break;
				case _nextButton:			doNextButton();								break;
			}
		}

		protected function showWebcamVideo($add:Boolean = true):void {
			if(_ppModel && _ppModel.webcamPicModel && _ppModel.webcamPicModel.video) {
				_ppModel.webcamPicModel.video.visible = $add;
			}
		}

		protected function doBackButton():void {
			switch(_currentStep) {
				case 0:					destructUserPicker();
										setYButtons();
										AnalyticManager.getInstance().setTrackEvent('escogeFoto', 'click', 'regresar');
										dispatchEvent(new Event(ON_HOME_CLICK));			return;
				case 1:					setYButtons();
										showHomeButtons(true);
										showWebcamVideo(false);
										showShotAgain(false);
										destructTimer();
										AnalyticManager.getInstance().setTrackEvent('ajustaFoto', 'click', 'regresar');
										showAdjustPhotoStage(null, false);
										showLastHead(false);
										_ppModel.fbPicModel.setState(BasicFormStates.CANCELING);
										break;
				case 2:					setYButtons(565);
										showAdjustPhotoStage(null, false);
										_adjustController.showAdjustViewer(false);
										_adjustController.activeCursor(false);
										if(_ppModel.currentState == UserPicturePickerStates.FB_PICTURE_MODE) {
											(_userPPView as BanamexPicturePickerGraphView).forceFBView();
										} else {
											_currentStep--;
											doBackButton();
											return;
										}
										break;
				case 3:					reinit();
										setYButtons();
//										showAdjustPhotoStage(null, true);
//										showSurpriseElements();
//										_adjustController.activeCursor();
//										_adjustController.viewer.dynamicImage.enabled = true;
										showGeneralFaceElements(false);
										dispatchEvent(new Event(HIDE_SHARE));
			}
			setCurrentStep(_currentStep - 1);
		}

		protected function doNextButton():void {
			switch(_currentStep) {
				case 1:								setYButtons(565);
													showShotAgain(false);
													showLastHead(false);
													showWebcamVideo(false);
													AnalyticManager.getInstance().setTrackEvent('escogeFoto', 'click', 'siguiente');
													doAdjustCurrentPhoto();
													_adjustController.activeCursor();
													return;
				case 2:								showAdjustPhotoStage(null, false);
													showShotAgain(false);
													_userImage = GraphicUtils.getClone(_adjustController.currentHead);
													AnalyticManager.getInstance().setTrackEvent('ajustaFoto', 'click', 'siguiente');
													showSurpriseElements();
													_adjustController.activeCursor(false);
													_adjustController.viewer.dynamicImage.enabled = false;
													showGeneralFaceElements(true);
													dispatchEvent(new Event(ON_HEAD_ADJUST));
													break;
				case 3:								saveCelebrationAlter();
													break;
			}
			setCurrentStep(_currentStep + 1);
		}

		protected function saveCelebrationAlter():void {
			var transactor:TransactionManager = new TransactionManager();
			addListener(transactor, TransactionManager.ON_SAVE_CELEBRATION_SUCCESS, onSaveSuccess);
			transactor.saveCelebration(_userImage);
		}

		protected function onSaveSuccess($event:BasicLoaderEvent):void {
			var transactor:TransactionManager = $event.currentTarget as TransactionManager;
			addListener(transactor, TransactionManager.ON_SAVE_CELEBRATION_SUCCESS, onSaveSuccess, false);
			_savedID = $event.serverResponse.id;
			setCurrentStep(0);
			dispatchEvent(new Event(ON_CELEBRATION_MADE));
			destructUserPicker();
		}

		protected function get alertsContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("alertsContainer");
		}

		protected function destructUserPicker():void {
			if(_userPPView) {
				trace("destructUserPicker :: " + _userPPView.parent);
				addListener(_ppModel, Event.CHANGE, onUserPicturePickerStateChange, false);
				_userPPView.destructor();
				_userPPView = null;
			}
		}

		protected function createUserPicturePicker():void {
			destructUserPicker();
			
			_ppModel = new UserPicturePickerGraphModel();
			var userFileModel:BasicFileReferenceModel = new BasicFileReferenceModel();
			var webcamModel:BasicWebcamModel = new BasicWebcamModel();
			
			webcamModel.videoWidth = 352;
			webcamModel.videoHeight = 264;

			var fbPicturePicker:BanamexFBGPhotoPickerModel = new BanamexFBGPhotoPickerModel();
			fbPicturePicker.photosLimit = 12;
			webcamModel.micEnabled = false;
			adjustGraphic.adjustHead.sliders.controlsBar.visible = false;
			webcamModel.videoContainer = _screenGraphic;
			_ppModel.userFileModel = userFileModel;
			_ppModel.webcamPicModel = webcamModel;
			_ppModel.fbPicModel = fbPicturePicker;
			addListener(_ppModel, Event.CHANGE, onUserPicturePickerStateChange, true, true);
			var ctrl:IController = new UserPicturePickerGraphController(_ppModel);
			(ctrl as UserPicturePickerGraphController).fileRefControllerClass = BasicFileReferenceControllerGraph;
			(ctrl as UserPicturePickerGraphController).fbControllerClass = BanamexFBGraphPicturePickerController;
			var graphics:Dictionary = new Dictionary();
			_userPPView = new  BanamexPicturePickerGraphView(_ppModel, ctrl, containerMC, graphics);
			(_userPPView as StandardPicturePickerGraphView).loaderFBPoint = new ComplexPoint(-70, 0);
			(_userPPView as StandardPicturePickerGraphView).fbGalleryW = 290;
			(_userPPView as StandardPicturePickerGraphView).fbGalleryX = 150;
			(_userPPView as StandardPicturePickerGraphView).fbGalleryY = 185;
			(_userPPView as StandardPicturePickerGraphView).webCamContainerPosition = new ComplexPoint(0, 0);//397, 315
			(_userPPView as StandardPicturePickerGraphView).webCamPreviewPosition = new ComplexPoint(0, 0);
			(_userPPView as StandardPicturePickerGraphView).gallerySelectorColor = 0xCC7B22;
		}

		protected function getImageMask($x:Number, $y:Number):DisplayObject {
			var mask:Sprite = new Sprite();
			mask.graphics.beginFill(0, 1);
			mask.graphics.drawRect($x, $y, 400, 350);
			mask.graphics.endFill();
			return mask;
		}

		protected function onUserPicturePickerStateChange($event:Event):void {
			var model:UserPicturePickerGraphModel = $event.target as UserPicturePickerGraphModel;
			trace(model.currentState);
			if(model.currentState == BasicFormStates.CANCELED || model.currentState == BasicFormStates.FINISHED || UserPicturePickerStates.HOME) {
				if(model.webcamPicModel.currentError && model.webcamPicModel.currentError != "" && model.currentState == UserPicturePickerStates.HOME && model.webcamPicModel.currentState == BasicFormStates.CANCELED) {
					showAdjustPhotoStage(null, false);
					if(_adjustController) {
//						_adjustController.setWebcamHolder(_webcamContainer, false);
					}
					showHomeButtons(true);
					setCurrentStep(0);
				}
			}
			if(model.currentState == BasicFormStates.FINISHED) {
				doAdjustCurrentPhoto();
			}
			if(model.currentState == UserPicturePickerStates.FB_PICTURE_MODE) {
				_capturePhoto = null;
				showLastHead(false);
				AnalyticManager.getInstance().setTrackEvent('escogeFoto', 'click', 'fb');
				if(_adjustController) {
//					_adjustController.setWebcamHolder(_webcamContainer, false);
				}
				showHomeButtons(false);
				showFBStage();
				if(_currentStep == 0) {
					setCurrentStep(1);
				}
				SiteAlertManager.getInstance().showLoader(true, "Obteniendo información de Facebook", System.stageRoot.stageWidth / 2, (System.stageRoot.stageHeight / 2) + 75, true);
			}
			if(model.currentState == UserPicturePickerStates.WEB_CAM_MODE) {
				setCurrentStep(1);
				showWebcamVideo();
				AnalyticManager.getInstance().setTrackEvent('escogeFoto', 'click', 'webcam');
				showHomeButtons(false);
				showAdjustPhotoStage();
//				_adjustController.setWebcamHolder(_webcamContainer);
			}
			setYButtons(565);
		}

		protected function doAdjustCurrentPhoto():void {
			showFBStage(false);
			if(_adjustController) {
//				_adjustController.setWebcamHolder(_webcamContainer, false);
			}
			showAdjustPhotoStage(_capturePhoto ? _capturePhoto : _ppModel.userImage);
			adjustGraphic.adjustHead.sliders.controlsBar.visible = true;
			setCurrentStep(2);
		}

		protected function showFBStage($add:Boolean = true):void {
			addElement(fbGraphic, $add);
		}

		protected function showAdjustPhotoStage($head:DisplayObject = null, $add:Boolean = true):void {
			if(!_adjustController) {
				_adjustController = new AdjustHeadController(adjustGraphic, _screenGraphic);
			}
			if($head) {
				_adjustController.setHead($head, _capturePhoto != null);
			}
			if(!$head && _ppModel.currentState == UserPicturePickerStates.FB_PICTURE_MODE) {
				_adjustController.showControllers($add);
			}
			addElement(_adjustController.container, $add);
		}

		protected function showHomeButtons($add:Boolean = true):void {
			if($add) {
//				_currentWebcamDO = null;
			}
			addElement(homeGraphic, $add);
		}

		protected function setCurrentStep($value:uint):void {
			_currentStep = Math.min(Math.max(0, $value), 3);
			addBackFowardButtons(_currentStep != 3, _currentStep == 2, _currentStep == 1 && _ppModel.currentState == UserPicturePickerStates.WEB_CAM_MODE);
		}

		protected function setCaptureTimer():void {
			showLastHead(false);
			_captureTimer = new Timer(1000, 3);
			addTimerListeners();
			_captureTimer.start();
			showCounter();
			counter.gotoAndStop("l" + (3 - _captureTimer.currentCount));
		}
		
		protected function destructTimer():void {
			showCounter(false);
			if(_captureTimer) {
				if(_captureTimer.running) {
					_captureTimer.stop();
				}
				addTimerListeners(false);
				_captureTimer = null;
			}
		}

		protected function addTimerListeners($add:Boolean = true):void {
			addListener(_captureTimer, TimerEvent.TIMER, onTimer, $add);
			addListener(_captureTimer, TimerEvent.TIMER_COMPLETE, onTimerComplete, $add);
		}
		
		protected function onTimer($event:TimerEvent):void {
			if(_captureTimer.currentCount != 3) {
				counter.gotoAndStop("l" + (3 - _captureTimer.currentCount));
			}
		}

		protected function showShotAgain($add:Boolean = true):void {
			addElement(_shotAgainButton, $add);
			addListener(_shotAgainButton, MouseEvent.CLICK, onButtonClick, $add);
			_backButton.x = $add ? 15 : 90;
			_shotAgainButton.x = _backButton.x + 132;
			_nextButton.x = $add ? _shotAgainButton.x + 165 : _backButton.x + 155;
			_nextButton.y = _backButton.y;
		}

		protected var _capturePhoto:DisplayObject;

		protected function onTimerComplete($event:TimerEvent):void {
			destructTimer();
			if(_capturePhoto) {
				addElement(_capturePhoto, false, _screenGraphic);
				_capturePhoto = null;
			}
			_capturePhoto = GraphicUtils.getClone(_screenGraphic);
			showLastHead();
			addBackFowardButtons(true, true, false);
			showShotAgain();
		}

		protected function showLastHead($add:Boolean = true):void {
			addElement(_capturePhoto, $add, _screenGraphic);
		}

		protected var _faceElements:Object;
		protected var _feLoader:GraphicLoadManager;
		protected var _eyesID:uint;
		protected var _mouthID:uint;
		protected var _currentEyes:Sprite;
		protected var _currentMouth:Sprite;
		protected var _eyesGallery:SimpleGallery;
		protected var _mouthGallery:SimpleGallery;
		protected var _faceElementIsEyes:Boolean = false;

		protected function showSurpriseElements($add:Boolean = true):void {
			if(!_faceElements && $add) {
				loadElements();
				_feLoader = new GraphicLoadManager();
				_currentEyes = new Sprite();
				_currentMouth = new Sprite();
			}
		}

		protected function addFaceElementsListeners($add:Boolean = true):void {
			_currentEyes.buttonMode = $add;
			_currentEyes.useHandCursor = $add;
			_currentMouth.buttonMode = $add;
			_currentMouth.useHandCursor = $add;
			addListener(_currentEyes, MouseEvent.CLICK, onFaceElementClick, $add);
			addListener(_currentMouth, MouseEvent.CLICK, onFaceElementClick, $add);
		}

		protected function onFaceElementClick($event:MouseEvent):void {
			addFaceElementsListeners(false);
			if($event.currentTarget == _currentEyes) {
				if(!_eyesGallery) {
					_eyesGallery = createGallery(getThumbnailData(_faceElements.eyes), _eyesGallery, true);
				} else {
					addElement(_eyesGallery, true, _screenGraphic);
				}
				_faceElementIsEyes = true;
			} else {
				if(!_mouthGallery) {
					_mouthGallery = createGallery(getThumbnailData(_faceElements.mouths), _mouthGallery, false);
				} else {
					addElement(_mouthGallery, true, _screenGraphic);
				}
				_faceElementIsEyes = false;
			}
		}

		protected function loadElements():void {
			var transactor:Banamex120HoursServicesController = new Banamex120HoursServicesController(null);
			addListener(transactor, Banamex120HoursServicesController.ON_COMPLETE, onDataComplete);
			transactor.getFaceElements();
		}

		protected function onDataComplete($event:BasicLoaderEvent):void {
			var transactor:Banamex120HoursServicesController = $event.target as Banamex120HoursServicesController;
			addListener(transactor, Banamex120HoursServicesController.ON_COMPLETE, onDataComplete, false);
			_faceElements = $event.serverResponse;
			_eyesID = MathUtils.randomRange(0, _faceElements.eyes.length - 1);
			_mouthID = MathUtils.randomRange(0, _faceElements.mouths.length - 1);
			loadCurrentElements();
		}

		protected function loadCurrentElements():void {
			trace(_eyesID + " ::: " + _mouthID);
			loadFaceElements(_faceElements.eyes[_eyesID], _faceElements.mouths[_mouthID]);
		}

		protected function loadFaceElements($eyes:Object, $mouth:Object):void {
			var pendientGraphic:Boolean = false;
			if(!_feLoader.library["eyes" + $eyes.ID]) { 
				_feLoader.loadRequest("eyes" + $eyes.ID, URLManager.getInstance().getPath("imagesDir") + $eyes.url, 97, 60);
				pendientGraphic = true;
			}
			if(!_feLoader.library["mouth" + $mouth.ID]) { 
				_feLoader.loadRequest("mouth" + $mouth.ID, URLManager.getInstance().getPath("imagesDir") + $mouth.url, 108, 148);
				pendientGraphic = true;
			}
			if(pendientGraphic) {
				addListener(_feLoader, GraphicLoadManagerEventType.ON_GRAPHICS_LOADED, onFaceElementsLoaded);
				_feLoader.startLoad();
			} else {
				onFaceElementsLoaded(null);
			}
		}

		protected function onFaceElementsLoaded($event:Event):void {
			if($event) {
				addListener(_feLoader, GraphicLoadManagerEventType.ON_GRAPHICS_LOADED, onFaceElementsLoaded, false);
			}
			destructCurrentFaceElements();
			_feLoader.library["eyes" + _faceElements.eyes[_eyesID].ID].scaleX = _feLoader.library["eyes" + _faceElements.eyes[_eyesID].ID].scaleY = .85;
			_feLoader.library["mouth" + _faceElements.mouths[_mouthID].ID].scaleX = _feLoader.library["mouth" + _faceElements.mouths[_mouthID].ID].scaleY = .85;
			addElement(_feLoader.library["eyes" + _faceElements.eyes[_eyesID].ID], true, _currentEyes);
			addElement(_feLoader.library["mouth" + _faceElements.mouths[_mouthID].ID], true, _currentMouth);
			showGeneralFaceElements();
		}

		protected function showGeneralFaceElements($add:Boolean = true):void {
			addElement(_currentEyes, $add, _screenGraphic);
			addElement(_currentMouth, $add, _screenGraphic);
			addFaceElementsListeners($add);
		}

		protected function createGallery($data:Array, $gallery:SimpleGallery, $eyes:Boolean = true):SimpleGallery {
			if(!$gallery) {
				$gallery = new SimpleGallery(-1, 1);
				$gallery.backgroundClass = $eyes ? SWCBackLiveMenu : SWCBackMouthsMenu;
				$gallery.thumbClass = BanamexElementFaceThumbnail;
				$gallery.adjustImage = true;
				$gallery.paddingY = 0;
				$gallery.x = $eyes ? 4 : 220;
				$gallery.y = $eyes ? 20 : 40;
				addListener($gallery, ThumbnailEventType.ON_PRESS, onThumbPress);
			}
			addElement($gallery, true, _screenGraphic);
			$gallery.initializeGallery($data.slice(0,8));
			return $gallery;
		}

		protected function onThumbPress($event:ThumbnailEvent):void {
			var newImage:DisplayObject = GraphicUtils.getClone(($event.thumb as BanamexElementFaceThumbnail).image);
			addElement($event.target as DisplayObject, false, _screenGraphic);
			if(_faceElementIsEyes) {
				_eyesID = uint($event.ID) - 1;
			} else {
				_mouthID = uint($event.ID) - 1;
			}
			loadCurrentElements();
		}

		protected function getThumbnailData($data:Array):Array {
			var data:Array = new Array();
			for(var i:uint = 0; i < $data.length; i++) {
				var thumb:ThumbnailData = new ThumbnailData($data[i].ID, $data[i].ID);
				thumb.thumbnailURL = URLManager.getInstance().getPath("imagesDir") + $data[i].url; 
				data.push(thumb);	
			}
			return data;
		}

		protected function destructCurrentFaceElements():void {
			if(_currentEyes.numChildren > 0) {
				addElement(_currentEyes.getChildAt(0), false, _currentEyes);
			}
			if(_currentMouth.numChildren > 0) {
				addElement(_currentMouth.getChildAt(0), false, _currentMouth);
			}
		}

	}

}