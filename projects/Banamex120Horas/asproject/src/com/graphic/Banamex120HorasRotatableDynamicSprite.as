package com.graphic {
	import com.core.system.System;
	import com.utils.graphics.DisplayContainer;
	import com.utils.graphics.MainDisplayController;
	
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import net.banamex.events.EtniaDataEvent;

	public class Banamex120HorasRotatableDynamicSprite extends RotatableDynamicSprite {
		protected var _helpSprite:Sprite;
		
		public function Banamex120HorasRotatableDynamicSprite()	{
			super();
		}
		
		override protected function onButtonMouseDown($event:MouseEvent):void {
			showHelpSprite();
			super.onButtonMouseDown($event);
		}
		
		override protected function onButtonMouseUp($event:MouseEvent):void {
			showHelpSprite(false);
			super.onButtonMouseUp($event);
		}
		
		override protected function addDragListeners($element:InteractiveObject, $down:Boolean = true, $destruct:Boolean = false):void {
			addListener($element, MouseEvent.MOUSE_DOWN, onButtonMouseDown, $down && !$destruct);
			addListener(_controlsContainer, Event.ENTER_FRAME, whileButtonPress, !$down && !$destruct, true);
			addListener(_helpSprite, MouseEvent.MOUSE_UP, onButtonMouseUp, !$down && !$destruct, true);
		}
		
		protected function showHelpSprite($add:Boolean = true):void {
			if(!_helpSprite) {
				_helpSprite = new Sprite();
				_helpSprite.graphics.beginFill(0xFFFFFF, 0);
				_helpSprite.graphics.drawRect(0, 0, System.stageRoot.width, System.stageRoot.height);
				_helpSprite.graphics.endFill();
			}
			addElement(_helpSprite, $add, alertsContainer);
		}
		
		protected function get alertsContainer():DisplayContainer {
			return MainDisplayController.getInstance().displayContainer.getStoredContainer("alertsContainer");
		}
	}
}