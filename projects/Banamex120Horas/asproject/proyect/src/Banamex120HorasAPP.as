package
{	
	import com.core.system.EnvironmentLoader;
	import com.core.system.System;
	import com.interfaces.IClient;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.LocalConnection;
	
	import net.banamex.Banamex120HoursMain;

	[SWF(width="450", height="680", frameRate="30", backgroundColor="#0093D2")]

	public class Banamex120HorasAPP extends EnvironmentLoader
	{
		public function Banamex120HorasAPP() {
			System.stageRoot.scaleMode = StageScaleMode.NO_SCALE;
			System.stageRoot.align = StageAlign.TOP_LEFT;
			System.version = "1.0.1";
			addListener(stage, KeyboardEvent.KEY_DOWN, onKeyboardPush);
//			addListener(stage.root, MouseEvent.MOUSE_UP, onMouseUp, true, true);
//			addListener(System.topRoot, MouseEvent.MOUSE_UP, onMouseUp, true, true);
		}
		
		protected function onMouseUp($event:MouseEvent):void {
			System.addDebugMessage("APP :: onMouseAction  :::  " + $event.type + "  :::  " + $event.currentTarget + "  :::  " + $event.currentTarget.name);
		}


		protected var _tempString:String = "";

		protected function onKeyboardPush($event:KeyboardEvent):void {
			_tempString = (_tempString + String.fromCharCode($event.keyCode.toString())).toLowerCase();
			var tokenShow:String = "showdevdebug";
			var tokenHide:String = "hidedevdebug";
			if(tokenShow.indexOf(_tempString) == 0 || tokenHide.indexOf(_tempString) == 0) {
				if(tokenShow == _tempString) {
					System.showDebbuger();
					_tempString = "";
				}
				if(tokenHide == _tempString) {
					System.hideDebbugger();
					_tempString = "";
				}
			} else {
				_tempString = "";
			}
		}

		override protected function getMain():IClient {
			return new Banamex120HoursMain(_environment.initConfigPath + _environment.URLSData);
		}

	}
}