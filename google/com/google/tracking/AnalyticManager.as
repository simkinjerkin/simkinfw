package com.google.tracking {

	import com.core.config.SystemConfigurationManager;
	import com.google.analytics.GATracker;
	
	import flash.display.DisplayObject;
	import flash.net.LocalConnection;

	public class AnalyticManager {

		private static var _instance:AnalyticManager;
		private static var _tracker:GATracker;

		public function AnalyticManager($class:PrivateClass){}

		public static function getInstance():AnalyticManager {
			if(_instance == null) {
				_instance = new AnalyticManager(new PrivateClass()); 
			} else {
				trace("You already have an instance of AnalyticManager which is being return.");
			}
			return _instance;
		}

		public function setTracking($display:DisplayObject, $propertyID:String, $mode:String, $debug:Boolean = false):void {
			if((new LocalConnection()).domain != "localhost") {
				if(_tracker == null) {				
					_tracker = new GATracker($display, $propertyID, $mode, $debug);
					_tracker.debug.destroyKey = 123;
					_tracker.debug.showHideKey = 120;
					_tracker.debug.minimizedOnStart = true;
				} else {
					throw new Error("AnalyticManager.getInstance().setTracking() can be Set only once;");
				}
			} else {
				trace("setTracking ::: " + arguments + "  ---> Localhost");
			}
		}

		public function setTrackPage($pageid:String):void {
			if((new LocalConnection()).domain != "localhost" && SystemConfigurationManager.getInstance().configurationData.enabledTracking) {
				if(_tracker) {
					_tracker.trackPageview($pageid);
				} else {
					throw new Error("AnalyticManager.getInstance().setTracking() should be set.");
				}
			} else {
				trace("SystemConfigurationManager.getInstance().configurationData.enabledTracking :: " + SystemConfigurationManager.getInstance().configurationData.enabledTracking);
			}
		}

		public function setTrackEvent($arg0:String, $arg1:String, $arg2:String = null, $arg3:Number = NaN):void {
			if((new LocalConnection()).domain != "localhost" && SystemConfigurationManager.getInstance().configurationData.enabledTracking) {
				if(_tracker) {
					_tracker.trackEvent($arg0, $arg1, $arg2, $arg3);
				} else {
					throw new Error("AnalyticManager.getInstance().setTracking() should be set.");
				}
			} else {
				trace("SystemConfigurationManager.getInstance().configurationData.enabledTracking :: " + SystemConfigurationManager.getInstance().configurationData.enabledTracking);
			}
		}
	}
}

class PrivateClass
{
	public function PrivateClass() {
		trace("AnalyticManager up and running");
	}
}